fun firstArg(callExpr) {
	ret callExpr.args(0);
}

fun time() {
	say(stdout, tick());
}

fun loop(N, code) {
	ret codefmt({:
		my j = %N;
		while j {
			%code;
			j = minus(j, 1);
		}
	:}, "N", N, "code", code);
}

fun timeloop(N, code) {
	ret codefmt({:
		time();
		%code;
		time();
	:}, "code", loop(N, code));
}

fun main() {
	my i = 0;
	
	@timeloop({= 10000000 =}, {: :});
	@timeloop({= 10000000 =}, {: i = @firstArg({= f(1) =}); :});
	@timeloop({= 10000000 =}, {: i = firstArg({= f(1) =}).value; :});
}
