import re

EXCLUDED = set((
	'Expr', 'Stmt', 'Suite'
))

def main(args):
	import os
	
	dir = 'mack/tree'
	
	for file in os.listdir(dir):
		filename = os.path.join(dir, file)
		
		if not file.endswith(".java"):
			continue
		
		if is_excluded(file):
			continue
		
		if needs_regen(filename):
			dynogen(filename)

def is_excluded(file):
	return file[0:-5] in EXCLUDED

def needs_regen(file):
	# TODO
	return True

def dynogen(file):
	(pre, post) = get_sections(file)
	generated = gen_methods(pre + post)
	output_file(file, pre, generated, post)

def get_sections(file):
	pre = []
	post = []
	
	state = 1
	
	with open(file, 'r') as fh:
		for line in fh:
			line = strip_trailing(line)
			if state == 1:
				if is_gen_begin(line):
					state = 2
				else:
					pre.append(line)
			elif state == 2:
				if is_gen_end(line):
					state = 3
			elif state == 3:
				post.append(line)
	
	if state == 1 and not post:
		for i in xrange(len(pre) - 1, -1, -1):
			if pre[i] == '}':
				break
		post = pre[i:]
		pre = pre[0:i]
	
	return (pre, post)

def strip_trailing(s):
	if s.isspace():
		return s.rstrip('\r\n')
	return s.rstrip()

def is_gen_begin(line):
	return bool(re.match(r'^\s*//#dynogen begin\b', line))

def is_gen_end(line):
	return bool(re.match(r'^\s*//#dynogen end\b', line))

def gen_methods(lines):
	fields = get_fields(lines)
	if fields:
		getField = gen_get_field(fields)
		setField = gen_set_field(fields)
		return getField + [''] + setField
	return []

def get_fields(lines):
	fields = []
	
	for line in lines:
		line = re.sub(r'\s+', ' ', line).strip()
		m = re.match(r'^public (?:final )?(\S+) (\S+)(?: = new [^;]+)?;$', line)
		
		if not m:
			continue
		
		field = (m.group(1), m.group(2))
		fields.append(field)
	
	return fields

def gen_get_field(fields):
	if not fields:
		return []
	r = []
	r.append('public Dyno getField(String name) {')
	for (_, name) in fields:
		r.append('\tif (name.equals("' + name + '")) return this.' + name + ';')
	r.append('\treturn super.getField(name);')
	r.append('}')
	return r

def gen_set_field(fields):
	if not fields:
		return []
	r = []
	
	has_generic = False
	for (type, _) in fields:
		if is_generic(type):
			has_generic = True
			break
	
	if has_generic:
		r.append('@SuppressWarnings("unchecked")')
	
	r.append('public void setField(String name, Dyno value) {')
	for (type, name) in fields:
		r.append('\tif (name.equals("' + name + '")) {')
		r.append('\t\tif (value instanceof ' + wildcard_generic(type) + ') {')
		r.append('\t\t\tthis.' + name + ' = (' + type + ')value;')
		r.append('\t\t\treturn;')
		r.append('\t\t}')
		r.append('\t\tthrow TypeError.SetInvalidType(name);')
		r.append('\t}')
	r.append('\tsuper.setField(name, value);');
	r.append('}')
	return r

def is_generic(type):
	return '<' in type

def wildcard_generic(type):
	return re.sub(r'<\w+>', '<?>', type)

def output_file(file, pre, generated, post):
	with open(file, 'wb') as fh:
		last_was_space = False
		for line in pre:
			fh.write(line + '\n')
			last_was_space = line.isspace()
		if generated:
			if not last_was_space:
				fh.write('\t\n')
			fh.write('\t//#dynogen begin\n')
			for line in generated:
				fh.write('\t' + line + '\n')
			fh.write('\t//#dynogen end\n')
		for line in post:
			fh.write(line + '\n')

if __name__ == '__main__':
	import sys
	main(sys.argv)
