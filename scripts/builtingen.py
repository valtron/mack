import re

rx = re.compile(r'(\w+)\.(\w+)\(([^)]+)\)')

def get_parts(input_str):
	m = rx.search(input_str)
	
	if m is not None:
		return m.group(1, 2, 3)
	
	return None
	
def get_arg_count(args):
	args = args.replace(' ', '')
	
	args = args.split(',')
	
	if len(args) == 0:
		return 0
	
	if args[-1] == '...':
		return -1
	
	return len(args)

def generate_class(input_str):
	parts = get_parts(input_str)
	if parts is None:
		print 'ERROR in ' + input_str
		return
	
	outer_class, class_name, arg_string = parts
	arg_count = get_arg_count(arg_string)
	
	arg_list = ''
	if arg_count != -1:
		count_check = '\t\t\t\tTypeError.CheckArgumentCount({}, args);'.format(arg_count)
		for i in xrange(arg_count):
			arg_list += '\t\t\t\tD arg{i} = (D) args.get({i});\n'.format(i = i)
	else:
		count_check = '\t\t\t\tD{c} ret = new D{c}();'.format(c = outer_class)
		arg_list = '\t\t\t\tfor (Dyno arg : args) {\n\t\t\t\t\t\n\t\t\t\t}\n\t\t\t\treturn ret;\n'
	
	
	print '''\
		private static class {class_name} extends ConstObj {{
			@Override
			public Dyno call(List<Dyno> args) {{
{count_check}
{arg_list}
			}}
		}}'''.format(class_name = class_name.title(), count_check = count_check, arg_list = arg_list)
		
def gc(l):
	for i in l.split('\n'):
		parts = get_parts(i)
		if parts is not None:
			_, funcname, _ = parts
			print '\t\t\t\tthis.setFieldOverride("{}", new {}());'.format(funcname, funcname.title())
			
	for i in l.split('\n'):
		generate_class(i)
