package mack.exec;

public class ExecError extends Error
{
	private static final long serialVersionUID = -6765936197771845450L;
	
	public static final ExecError UnexpandedMacro = new ExecError("unexpanded macro");
	public static final ExecError UnexpandedPlaceholder = new ExecError("unexpanded placeholder");
	
	public ExecError(String message) {
		super(message);
	}
}
