package mack.exec;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class DInFile extends DNative
{
	private final BufferedReader reader;
	
	public DInFile(String filename) throws FileNotFoundException {
		this.reader = new BufferedReader(
			new FileReader(filename)
		);
	}
	
	public DInFile(InputStream stream) {
		this.reader = new BufferedReader(
			new InputStreamReader(stream)
		);
	}
	
	public String readLine() throws IOException {
		return this.reader.readLine();
	}
}
