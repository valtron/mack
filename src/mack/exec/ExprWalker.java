package mack.exec;

import mack.tree.Call;
import mack.tree.FieldGet;
import mack.tree.MacroExpr;
import mack.tree.PlaceExpr;
import mack.tree.Value;
import mack.tree.Var;

public interface ExprWalker<T> {
	public T walk(Call obj);
	public T walk(FieldGet obj);
	public T walk(MacroExpr obj);
	public T walk(PlaceExpr obj);
	public T walk(Value obj);
	public T walk(Var obj);
}
