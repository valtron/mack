package mack.exec;

class CompilationError extends Error
{
	private static final long serialVersionUID = -1750300666370345972L;
	
	public CompilationError(String message) {
		super(message);
	}
}
