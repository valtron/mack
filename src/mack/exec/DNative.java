package mack.exec;

import java.util.List;

public abstract class DNative implements Dyno
{
	@Override
	public Dyno getField(String name) {
		throw TypeError.HasNotField(name);
	}

	@Override
	public void setField(String name, Dyno value) {
		throw TypeError.SetNonExistentField(name);
	}
	
	@Override
	public Dyno call(List<Dyno> args) {
		throw TypeError.NotAFunction();
	}
}
