package mack.exec;

public class DString extends DNative
{
	public final String value;
	
	public DString(String value) {
		this.value = value;
	}
}
