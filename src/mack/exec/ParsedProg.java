package mack.exec;

import java.util.ArrayList;
import java.util.List;

import mack.parser.Parser;
import mack.tree.Suite;

public class ParsedProg
{
	public List<Suite> items = new ArrayList<Suite>();
	
	public static ParsedProg Parse(String file) {
		return Parser.Instance.parse(file);
	}
	
	public CompiledProg compile() {
		Compiler compiler = Compiler.Instance;
		return compiler.compile(this);
	}
}
