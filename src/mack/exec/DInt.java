package mack.exec;

public class DInt extends DNative
{
	public final int value;
	
	private DInt(int value) {
		this.value = value;
	}
	
	public static DInt Wrap(int value) {
		return new DInt(value);
	}
}
