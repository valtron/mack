package mack.exec;

import java.util.ArrayList;
import java.util.List;

import mack.tree.Args;
import mack.tree.Assign;
import mack.tree.Block;
import mack.tree.Call;
import mack.tree.Expr;
import mack.tree.ExprStmt;
import mack.tree.FieldGet;
import mack.tree.FieldSet;
import mack.tree.If;
import mack.tree.MacroExpr;
import mack.tree.MacroStmt;
import mack.tree.My;
import mack.tree.PlaceExpr;
import mack.tree.PlaceStmt;
import mack.tree.Ret;
import mack.tree.Stmt;
import mack.tree.Use;
import mack.tree.Value;
import mack.tree.Var;
import mack.tree.While;

public class MacroExpandWalker implements StmtWalker<Stmt>, ExprWalker<Expr>
{
	private final Env e;
	
	public MacroExpandWalker(Env e) {
		this.e = e;
	}
	
	@Override
	public Stmt walk(Assign obj) {
		Expr mValue = obj.value.accept(this);
		
		if (mValue == obj.value) {
			return obj;
		}
		
		return new Assign(obj.var, mValue);
	}
	
	@Override
	public Stmt walk(Block obj) {
		List<Stmt> mStmts = new ArrayList<Stmt>();
		
		boolean nothingChanged = true;
		
		for (Stmt stmt: obj.stmts) {
			Stmt mStmt = stmt.accept(this);
			mStmts.add(mStmt);
			
			if (mStmt != stmt) {
				nothingChanged = false;
			}
		}
		
		if (nothingChanged) {
			return obj;
		}
		
		return new Block(mStmts);
	}
	
	@Override
	public Stmt walk(FieldSet obj) {
		Expr mObj = obj.obj.accept(this);
		Expr mValue = obj.value.accept(this);
		
		if (mObj == obj.obj && mValue == obj.value) {
			return obj;
		}
		
		return new FieldSet(mObj, obj.field, mValue);
	}
	
	@Override
	public Stmt walk(If obj) {
		Expr mCond = obj.cond.accept(this);
		Stmt mThenStmt = obj.thenStmt.accept(this);
		Stmt mElseStmt = null;
		
		if (obj.elseStmt != null) {
			mElseStmt = obj.elseStmt.accept(this);
		}
		
		if (mCond == obj.cond && mThenStmt == obj.thenStmt && mElseStmt == obj.elseStmt) {
			return obj;
		}
		
		return new If(mCond, mThenStmt, mElseStmt);
	}
	
	@Override
	public Stmt walk(MacroStmt obj) {
		Expr e = obj.expr.accept(this);
		Dyno stmt = ExecUtils.Execute(this.e, e);
		
		if (!(stmt instanceof Stmt)) {
			throw new TypeError("macro used as statement did not produce a statement");
		}
		
		return (Stmt)stmt;
	}
	
	@Override
	public Stmt walk(My obj) {
		Expr mValue = obj.value.accept(this);
		
		if (mValue == obj.value) {
			return obj;
		}
		
		return new My(obj.varName, mValue);
	}
	
	@Override
	public Stmt walk(Ret obj) {
		if (obj.value == null) {
			return obj;
		}
		
		Expr mValue = obj.value.accept(this);
		
		if (mValue == obj.value) {
			return obj;
		}
		
		return new Ret(mValue);
	}
	
	@Override
	public Stmt walk(Use obj) {
		Expr mAliased = obj.aliased.accept(this);
		
		if (mAliased == obj.aliased) {
			return obj;
		}
		
		return new Use(obj.alias, mAliased);
	}
	
	@Override
	public Stmt walk(While obj) {
		Expr mCond = obj.cond.accept(this);
		Stmt mBody = obj.body.accept(this);
		
		if (mCond == obj.cond && mBody == obj.body) {
			return obj;
		}
		
		return new While(mCond, mBody);
	}

	@Override
	public Stmt walk(ExprStmt obj) {
		Expr mExpr = obj.expr.accept(this);
		
		if (mExpr == obj.expr) {
			return obj;
		}
		
		return new ExprStmt(mExpr);
	}

	@Override
	public Stmt walk(PlaceStmt obj) {
		Expr mExpr = obj.expr.accept(this);
		
		if (mExpr == obj.expr) {
			return obj;
		}
		
		return new PlaceStmt(mExpr);
	}

	@Override
	public Expr walk(Call obj) {
		Expr mObj = obj.obj.accept(this);
		List<Expr> mArgs = new ArrayList<Expr>();
		
		boolean nothingChanged = (mObj == obj);
		
		for (Expr arg: obj.args) {
			Expr mArg = arg.accept(this);
			mArgs.add(mArg);
			
			if (mArg != arg) {
				nothingChanged = false;
			}
		}
		
		if (nothingChanged) {
			return obj;
		}
		
		return new Call(mObj, new Args(mArgs));
	}
	
	@Override
	public Expr walk(FieldGet obj) {
		Expr mObj = obj.obj.accept(this);
		
		if (mObj == obj.obj) {
			return obj;
		}
		
		return new FieldGet(mObj, obj.field);
	}
	
	@Override
	public Expr walk(MacroExpr obj) {
		Expr e = obj.expr.accept(this);
		Dyno expr = ExecUtils.Execute(this.e, e);
		
		if (!(expr instanceof Expr)) {
			throw new TypeError("macro used as expression did not produce an expression");
		}
		
		return (Expr)expr;
	}
	
	@Override
	public Expr walk(Value obj) {
		if (obj.value instanceof Expr) {
			Expr mExpr = ((Expr)obj.value).accept(this);
			
			if (mExpr == obj.value) {
				return obj;
			}
			
			return new Value(mExpr);
		} else if (obj.value instanceof Stmt) {
			Stmt mStmt = ((Stmt)obj.value).accept(this);
			
			if (mStmt == obj.value) {
				return obj;
			}
			
			return new Value(mStmt);
		} else {
			return obj;
		}
	}
	
	@Override
	public Expr walk(Var obj) {
		return obj;
	}

	@Override
	public Expr walk(PlaceExpr obj) {
		Expr mExpr = obj.expr.accept(this);
		
		if (mExpr == obj.expr) {
			return obj;
		}
		
		return new PlaceExpr(mExpr);
	}
}
