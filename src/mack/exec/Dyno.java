package mack.exec;

import java.util.List;

// Dynamic object
public interface Dyno
{
	public Dyno getField(String name);
	public void setField(String name, Dyno value);
	public Dyno call(List<Dyno> args);
}
