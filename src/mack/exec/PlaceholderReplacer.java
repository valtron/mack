package mack.exec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mack.tree.Args;
import mack.tree.Assign;
import mack.tree.Block;
import mack.tree.Call;
import mack.tree.Expr;
import mack.tree.ExprStmt;
import mack.tree.FieldGet;
import mack.tree.FieldSet;
import mack.tree.If;
import mack.tree.MacroExpr;
import mack.tree.MacroStmt;
import mack.tree.My;
import mack.tree.PlaceExpr;
import mack.tree.PlaceStmt;
import mack.tree.Ret;
import mack.tree.Stmt;
import mack.tree.Use;
import mack.tree.Value;
import mack.tree.Var;
import mack.tree.While;

public class PlaceholderReplacer implements StmtWalker<Stmt>, ExprWalker<Expr>
{
	private final Map<String, Dyno> map;
	
	public PlaceholderReplacer(Map<String, Dyno> map) {
		this.map = map;
	}
	
	@Override
	public Stmt walk(Assign obj) {
		return new Assign(obj.var, obj.value.accept(this));
	}
	
	@Override
	public Stmt walk(Block obj) {
		List<Stmt> mStmts = new ArrayList<Stmt>();
		
		for (Stmt stmt: obj.stmts) {
			Stmt mStmt = stmt.accept(this);
			mStmts.add(mStmt);
		}
		
		return new Block(mStmts);
	}
	
	@Override
	public Stmt walk(FieldSet obj) {
		return new FieldSet(obj.obj.accept(this), obj.field, obj.value.accept(this));
	}
	
	@Override
	public Stmt walk(If obj) {
		Stmt mThenStmt = obj.thenStmt.accept(this);
		Stmt mElseStmt = null;
		
		if (obj.elseStmt != null) {
			mElseStmt = obj.elseStmt.accept(this);
		}
		
		return new If(obj.cond.accept(this), mThenStmt, mElseStmt);
	}
	
	@Override
	public Stmt walk(MacroStmt obj) {
		throw ExecError.UnexpandedMacro;
	}
	
	@Override
	public Stmt walk(PlaceStmt obj) {
		if (!(obj.expr instanceof Var)) {
			throw new NotImplementedError("complex placeholders not implemented");
		}
		
		String name = ((Var)obj.expr).name.value;
		
		if (!this.map.containsKey(name)) {
			throw new ExecError("missing mapping for placeholder '" + name + "'");
		}
		
		Dyno replacement = this.map.get(name);
		
		if (!(replacement instanceof Stmt)) {
			throw new TypeError("placeholder did not expand to stmt");
		}
		
		return (Stmt)replacement;
	}
	
	@Override
	public Stmt walk(My obj) {
		return new My(obj.varName, obj.value.accept(this));
	}
	
	@Override
	public Stmt walk(Ret obj) {
		if (obj.value == null) {
			return obj;
		}
		return new Ret(obj.value.accept(this));
	}
	
	@Override
	public Stmt walk(Use obj) {
		// Uses are supposed to be static
		return obj;
	}
	
	@Override
	public Stmt walk(While obj) {
		Stmt mBody = obj.body.accept(this);
		return new While(obj.cond.accept(this), mBody);
	}
	
	@Override
	public Stmt walk(ExprStmt obj) {
		return new ExprStmt(obj.expr.accept(this));
	}
	
	@Override
	public Expr walk(Call obj) {
		Args mArgs = new Args();
		
		for (Expr arg: obj.args) {
			mArgs.add(arg.accept(this));
		}
		
		return new Call(obj.obj.accept(this), mArgs);
	}
	
	@Override
	public Expr walk(FieldGet obj) {
		return new FieldGet(obj.accept(this), obj.field);
	}
	
	@Override
	public Expr walk(MacroExpr obj) {
		throw ExecError.UnexpandedMacro;
	}

	@Override
	public Expr walk(PlaceExpr obj) {
		if (!(obj.expr instanceof Var)) {
			throw new NotImplementedError("complex placeholders not implemented");
		}
		
		String name = ((Var)obj.expr).name.value;
		
		if (!this.map.containsKey(name)) {
			throw new ExecError("missing mapping for placeholder '" + name + "'");
		}
		
		Dyno replacement = this.map.get(name);
		
		if (!(replacement instanceof Expr)) {
			throw new TypeError("placeholder did not expand to expr");
		}
		
		return (Expr)replacement;
	}

	@Override
	public Expr walk(Value obj) {
		// TODO: What should replacers do to quoted code within quoted code?
		return obj;
	}

	@Override
	public Expr walk(Var obj) {
		return obj;
	}
}
