package mack.exec;

public class NotImplementedError extends Error
{
	private static final long serialVersionUID = 4315794525103318992L;
	
	public NotImplementedError(String message) {
		super(message);
	}
}
