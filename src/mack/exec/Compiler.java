package mack.exec;

import mack.exec.ParsedProg;

public class Compiler
{
	public final static Compiler Instance = new Compiler();
	
	private Compiler() {}
	
	public CompiledProg compile(ParsedProg p) {
		return CompilationUtils.Compile(p);
	}
}
