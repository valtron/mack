package mack.exec;

import java.util.HashMap;
import java.util.Map;

class EnvImpl implements Env {
	protected final Env parent;
	protected final Map<String, Dyno> data = new HashMap<String, Dyno>();
	
	public EnvImpl() {
		this.parent = null;
	}
	
	public EnvImpl(Env parent) {
		this.parent = parent;
	}
	
	public Dyno get(String name) {
		if (this.data.containsKey(name)) {
			return this.data.get(name);
		}
		
		if (this.parent != null) {
			return this.parent.get(name);
		}
		
		return null;
	}
	
	public void declare(String name, Dyno obj) {
		if (this.data.containsKey(name)) {
			throw new CompilationError("variable '" + name + "' redeclared");
		}
		this.data.put(name, obj);
	}
	
	public void set(String name, Dyno obj) {
		if (this.data.containsKey(name)) {
			this.data.put(name, obj);
		} else if (this.parent == null) {
			throw new CompilationError("cannot set undeclared variable '" + name + "'");
		} else {
			this.parent.set(name, obj);
		}
	}
}
