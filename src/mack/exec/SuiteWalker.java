package mack.exec;

import mack.tree.Func;
import mack.tree.MacroSuite;
import mack.tree.Module;
import mack.tree.My;
import mack.tree.Use;

public interface SuiteWalker {
	public void walk(Func obj);
	public void walk(MacroSuite obj);
	public void walk(Module obj);
	public void walk(My obj);
	public void walk(Use obj);
}
