package mack.exec;

public class DBool extends DNative
{
	public final static DBool True = new DBool(true);
	public final static DBool False = new DBool(false);
	
	public final boolean value;
	
	private DBool(boolean value) {
		this.value = value;
	}
	
	public static DBool Wrap(boolean value) {
		return (value ? True : False);
	}
}
