package mack.exec;

class EnvUtils {
	private EnvUtils() {}
	
	public static Env New(Env parent) {
		return new EnvImpl(parent);
	}
	
	public static Env WriteProtect(Env parent) {
		return new ProtectedEnv(parent);
	}
}
