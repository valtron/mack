package mack.exec;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mack.tree.Expr;
import mack.tree.Stmt;

public class BuiltIns
{
	private BuiltIns() {}
	
	public static Env GetRootEnv() {
		Env env = EnvUtils.New(null);
		
		Bool bool = new Bool();
		Int $int = new Int();
		Str str = new Str();
		File file = new File();
		
		env.declare("new", new New());
		env.declare("null", DNull.Instance);
		env.declare("tick", new Tick());
		env.declare("codefmt", new CodeFormat());
		env.declare("Bool", bool);
		env.declare("Int", $int);
		env.declare("Str", str);
		env.declare("List", new ListObj());
		env.declare("File", file);
		env.declare("Mack", new Mack());
		
		env.declare("lt", $int.getField("lt"));
		env.declare("plus", $int.getField("plus"));
		env.declare("minus", $int.getField("minus"));
		env.declare("times", $int.getField("times"));
		env.declare("iszero", $int.getField("iszero"));
		env.declare("true", bool.getField("True"));
		env.declare("false", bool.getField("False"));
		env.declare("and", bool.getField("and"));
		env.declare("or", bool.getField("or"));
		env.declare("not", bool.getField("not"));
		env.declare("concat", str.getField("concat"));
		env.declare("say", file.getField("say"));
		env.declare("write", file.getField("write"));
		env.declare("Open", file.getField("Open"));
		env.declare("stdout", file.getField("StdOut"));
		env.declare("stdin", file.getField("StdIn"));
		
		return env;
	}
	
	// new("foo", 1, "bar", 23, ...): returns a new object
	private static class New extends DNative
	{
		private final static TypeError InvalidCall = new TypeError("new expects an even number of arguments, as (string, obj) pairs");
		
		@Override
		public Dyno call(List<Dyno> args) {
			if (args.size() % 2 != 0) {
				throw New.InvalidCall;
			}
			
			Map<String, Dyno> fields = new HashMap<String, Dyno>();
			
			for (int i = 0; i < args.size(); i += 2) {
				Dyno name = args.get(i);
				Dyno value = args.get(i + 1);
				
				if (!(name instanceof DString)) {
					throw New.InvalidCall;
				}
				
				fields.put(((DString)name).value, value);
			}
			
			return new Obj(fields);
		}
	}
	
	private static class Tick extends DNative
	{
		@Override
		public Dyno call(List<Dyno> args) {
			TypeError.CheckArgumentCount(0, args);
			return new DString(System.currentTimeMillis() + "");
		}
	}
	
	// codefmt(code, "a", x, "b", y, ...)
	private static class CodeFormat extends DNative
	{
		private final static TypeError InvalidCall = new TypeError("codefmt expects a stmt or expr, followed by key-value mappings for placeholders");
		
		@Override
		public Dyno call(List<Dyno> args) {
			int n = args.size();
			
			if (n < 1 || n % 2 != 1) {
				throw InvalidCall;
			}
			
			Dyno arg0 = args.get(0);
			
			if (!(arg0 instanceof Stmt) && !(arg0 instanceof Expr)) {
				throw InvalidCall;
			}
			
			Map<String, Dyno> map = new HashMap<String, Dyno>();
			
			for (int i = 1; i < n; i += 2) {
				Dyno k = args.get(i);
				if (!(k instanceof DString)) throw InvalidCall;
				Dyno v = args.get(i + 1);
				if (!(v instanceof Stmt) && !(v instanceof Expr)) throw InvalidCall;
				map.put(((DString)k).value, v);
			}
			
			PlaceholderReplacer replacer = new PlaceholderReplacer(map);
			
			if (arg0 instanceof Stmt) {
				return ((Stmt)arg0).accept(replacer);
			} else {
				return ((Expr)arg0).accept(replacer);
			}
		}
	}
	
	private static class Obj extends DNative
	{
		protected final Map<String, Dyno> fields;
		
		public Obj() {
			this.fields = new HashMap<String, Dyno>();
		}
		
		public Obj(Map<String, Dyno> fields) {
			this.fields = fields;
		}
		
		@Override
		public Dyno getField(String field) {
			if (this.fields.containsKey(field)) {
				return this.fields.get(field);
			}
			throw TypeError.HasNotField(field);
		}
		
		@Override
		public void setField(String field, Dyno value) {
			if (this.fields.containsKey(field)) {
				this.fields.put(field, value);
				return;
			}
			throw TypeError.SetNonExistentField(field);
		}
	}
	
	private static abstract class ConstObj extends Obj
	{
		@Override
		public void setField(String field, Dyno value) {
			throw TypeError.SetReadonlyField(field);
		}
		
		public void setFieldOverride(String field, Dyno value) {
			this.fields.put(field, value);
		}
	}
	
	private static class Bool extends ConstObj
	{
		public Bool() {
			this.setFieldOverride("True", DBool.True);
			this.setFieldOverride("False", DBool.False);
			this.setFieldOverride("and", new And());
			this.setFieldOverride("or", new Or());
			this.setFieldOverride("not", new Not());
		}
		
		@Override
		public Dyno call(List<Dyno> args) {
			if (args.size() != 1) {
				throw TypeError.ExpectedArgumentCount(1, args.size());
			}
			
			return DBool.Wrap(ExecUtils.IsTrue(args.get(0)));
		}
		
		private static class And extends ConstObj
		{
			@Override
			public Dyno call(List<Dyno> args) {
				for (Dyno d: args) {
					if (!ExecUtils.IsTrue(d)) {
						return DBool.False;
					}
				}
				
				return DBool.True;
			}
		}
		
		private static class Or extends ConstObj
		{
			@Override
			public Dyno call(List<Dyno> args) {
				for (Dyno d: args) {
					if (ExecUtils.IsTrue(d)) {
						return DBool.True;
					}
				}
				
				return DBool.False;
			}
		}
		
		private static class Not extends ConstObj
		{
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(1, args);
				return DBool.Wrap(!ExecUtils.IsTrue(args.get(0)));
			}
		}
	}
	
	private static class Int extends ConstObj
	{
		private Int() {
			this.setFieldOverride("lt", new LT());
			this.setFieldOverride("plus", new Plus());
			this.setFieldOverride("minus", new Minus());
			this.setFieldOverride("times", new Times());
			this.setFieldOverride("iszero", new IsZero());
		}
		
		private static void CheckIntArgs(List<Dyno> args) {
			int i = 0;
			for (Dyno d: args) {
				i += 1;
				if (!(d instanceof DInt)) {
					throw TypeError.ArgumentInvalidType(i, "int");
				}
			}
		}
		
		private static abstract class Bin extends ConstObj
		{
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(2, args);
				CheckIntArgs(args);
				DInt a = (DInt)args.get(0);
				DInt b = (DInt)args.get(1);
				return this.call2int(a.value, b.value);
			}
			
			protected abstract Dyno call2int(int a, int b);
		}
		
		private static class LT extends Bin
		{
			@Override
			public Dyno call2int(int a, int b) {
				return DBool.Wrap(a < b);
			}
		}
		
		private static class Plus extends Bin
		{
			@Override
			public Dyno call2int(int a, int b) {
				return DInt.Wrap(a + b);
			}
		}
		
		private static class Minus extends Bin
		{
			@Override
			public Dyno call2int(int a, int b) {
				return DInt.Wrap(a - b);
			}
		}
		
		private static class Times extends Bin
		{
			@Override
			public Dyno call2int(int a, int b) {
				return DInt.Wrap(a * b);
			}
		}
		
		private static class IsZero extends ConstObj
		{
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(1, args);
				CheckIntArgs(args);
				DInt a = (DInt)args.get(0);
				return DBool.Wrap(a.value == 0);
			}
		}
	}
	
	private static class Str extends ConstObj
	{
		private Str() {
			this.setFieldOverride("len", new Len());
			this.setFieldOverride("concat", new Concat());
		}
		
		private static void CheckStrArgs(List<Dyno> args) {
			int i = 0;
			for (Dyno d : args) {
				i += 1;
				if (!(d instanceof DString)) {
					throw TypeError.ArgumentInvalidType(i, "string");
				}
			}
		}
		
		private static class Len extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(1, args);
				CheckStrArgs(args);
				return DInt.Wrap(((DString) args.get(0)).value.length());
			}
		}
		
		private static class Concat extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(2, args);
				CheckStrArgs(args);
				DString a = (DString) args.get(0);
				DString b = (DString) args.get(1);
				return new DString(a.value + b.value);
			}
		}
	}
	
	private static class ListObj extends ConstObj
	{
		private ListObj() {
			this.setFieldOverride("New", new New());
			this.setFieldOverride("size", new Size());
			this.setFieldOverride("add", new Add());
			this.setFieldOverride("set", new Set());
			this.setFieldOverride("insert", new Insert());
			this.setFieldOverride("Join", new Join());
		}
		
		private static void CheckListArgs(List<Dyno> args) {
			int i = 0;
			for (Dyno d : args) {
				i += 1;
				if (!(d instanceof DList<?>)) {
					throw TypeError.ArgumentInvalidType(i, "list");
				}
			}
		}
		
		private static class New extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				DList<Dyno> ret = new DList<Dyno>();
				
				for (Dyno arg : args) {
					ret.add(arg);
				}
				
				return ret;
			}
		}
		
		private static class Size extends ConstObj {
			@SuppressWarnings("unchecked")
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(1, args);
				CheckListArgs(args);
				DList<Dyno> arg0 = (DList<Dyno>) args.get(0);
				return DInt.Wrap(arg0.size());
			}
		}
		
		private static class Add extends ConstObj {
			@SuppressWarnings("unchecked")
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(2, args);
				TypeError.CheckArgumentTypes(args, DList.class, Dyno.class);
				
				DList<Dyno> arg0 = (DList<Dyno>) args.get(0);
				Dyno arg1 = args.get(1);
				arg0.add(arg1);
				
				return DNull.Instance;
			}
		}
		
		private static class Set extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(3, args);
				TypeError.CheckArgumentTypes(args, DList.class, DInt.class, Dyno.class);
				
				@SuppressWarnings("unchecked")
				DList<Dyno> arg0 = (DList<Dyno>) args.get(0);
				DInt arg1 = (DInt) args.get(1);
				Dyno arg2 = args.get(2);
				
				arg0.set(arg1.value, arg2);
				
				return arg0;
			}
		}
		
		private static class Insert extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(3, args);
				TypeError.CheckArgumentTypes(args, DList.class, DInt.class, Dyno.class);
				@SuppressWarnings("unchecked")
				DList<Dyno> arg0 = (DList<Dyno>) args.get(0);
				DInt arg1 = (DInt) args.get(1);
				Dyno arg2 = args.get(2);
				
				arg0.add(arg1.value, arg2);
				
				return arg0;

			}
		}
		
		private static class Join extends ConstObj {
			@SuppressWarnings("unchecked")
			@Override
			public Dyno call(List<Dyno> args) {
				CheckListArgs(args);
				DList<Dyno> ret = new DList<Dyno>();
				for (Dyno arg : args) {
					for (Dyno d : (DList<Dyno>)arg) {
						ret.add(d);
					}
				}
				return ret;
			}
		}
	}
	
	private static class File extends ConstObj
	{
		private File() {
			this.setFieldOverride("StdIn", new DInFile(System.in));
			this.setFieldOverride("StdOut", new DOutFile(System.out));
			this.setFieldOverride("write", new Write());
			this.setFieldOverride("read", new Read());
			this.setFieldOverride("say", new Say());
			this.setFieldOverride("Read", new ReadFile());
			this.setFieldOverride("Open", new Open());
		}
		
		private static class Write extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckMinimumArgumentCount(2, args);
				if (!(args.get(0) instanceof DOutFile)) {
					throw TypeError.ArgumentInvalidType(0, "ostream");
				}
				
				DOutFile fh = (DOutFile) args.get(0);
				
				int i = 1;
				for (Dyno arg : args.subList(1, args.size())) {
					i++;
					
					if (!(arg instanceof DString)) {
						throw TypeError.ArgumentInvalidType(i, "string");
					}
					
					try {
						fh.write(((DString)arg).value);
					} catch (IOException e) {
						throw new ExecError("io error during write");
					}
				}
				return fh;
			}
		}
		
		private static class Read extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(1, args);
				
				if (!(args.get(0) instanceof DInFile)) {
					throw TypeError.ArgumentInvalidType(0, "istream");
				}
				
				DInFile arg0 = (DInFile) args.get(0);
				
				try {
					return new DString(arg0.readLine());
				} catch (IOException e) {
					throw new ExecError("io error during read line");
				}
			}
		}
		
		private static class Say extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckMinimumArgumentCount(2, args);
				if (!(args.get(0) instanceof DOutFile)) {
					throw TypeError.ArgumentInvalidType(0, "ostream");
				}
				
				DOutFile fh = (DOutFile) args.get(0);

				int i = 1;
				for (Dyno arg : args.subList(1, args.size())) {
					i++;
					
					if (!(arg instanceof DString)) {
						throw TypeError.ArgumentInvalidType(i, "string");
					}
					
					try {
						fh.write(((DString)arg).value);
						fh.write("\n");
					} catch (IOException e) {
						throw new ExecError("io error during 'say'");
					}
				}
				
				return fh;
			}
		}
		
		private static class ReadFile extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(1, args);
				if (!(args.get(0) instanceof DString)) {
					throw TypeError.ArgumentInvalidType(0, "string");
				}
				
				DString arg0 = (DString) args.get(0);
				String filename = arg0.value;
				
				try {
					return new DString(ExecUtils.Slurp(filename));
				} catch (FileNotFoundException e) {
					throw new ExecError("file not found '" + filename + "'");
				} catch (IOException e) {
					throw new ExecError("io error during read of '" + filename + "'");
				}
			}
		}
		
		private static class Open extends ConstObj {
			@Override
			public Dyno call(List<Dyno> args) {
				TypeError.CheckArgumentCount(2, args);
				
				DString arg0 = (DString) args.get(0);
				DString arg1 = (DString) args.get(1);
				
				String filename = arg0.value;
				String mode = arg1.value;
				
				try {
					if ("r".equals(mode)) {
						return new DInFile(filename);
					} else if ("w".equals(mode)) {
						return new DOutFile(filename);
					}
					
					throw new TypeError("invalid mode '" + mode + "'");
				} catch (FileNotFoundException e) {
					throw new ExecError("file not found '" + filename + "'");
				} catch (IOException e) {
					throw new ExecError("io error while opening '" + filename + "'");
				}
			}
		}
	}
	
	private static class Mack extends ConstObj
	{
		private Mack() {
			// TODO: Mack builtins
			// Mack.Tree.Args(a1, a2, ...)
			// Mack.Tree.Assign(var, value)
			// etc.
		}
	}
}
