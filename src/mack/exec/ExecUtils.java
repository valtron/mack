package mack.exec;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import mack.tree.Expr;
import mack.tree.Stmt;

class ExecUtils {
	private ExecUtils() {}
	
	public static Dyno Execute(Env env, Stmt s) {
		Res res = s.accept(new ExecWalker(EnvUtils.WriteProtect(env)));
		return res.value;
	}
	
	public static Dyno Execute(Env env, Expr e) {
		Res res = e.accept(new ExecWalker(EnvUtils.WriteProtect(env)));
		return res.value;
	}
	
	public static boolean IsTrue(Dyno obj) {
		if (obj instanceof DBool) {
			return ((DBool)obj).value;
		} else if (obj instanceof DInt) {
			return ((DInt)obj).value != 0;
		} else if (obj instanceof DNull) {
			return false;
		} else if (obj instanceof DString) {
			return ((DString)obj).value.length() > 0;
		} else if (obj instanceof DList<?>) {
			return ((DList<?>)obj).size() > 0;
		} else {
			return true;
		}
	}
	
	public static String Slurp(String filename) throws IOException {
		BufferedReader reader = new BufferedReader(
				new FileReader(filename)
		);
		
		StringBuilder builder = new StringBuilder();
		String line = reader.readLine();
		while (line != null) {
			builder.append(line);
			line = reader.readLine();
		}
		reader.close();
		return builder.toString();
	}
}
