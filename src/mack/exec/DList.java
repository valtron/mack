package mack.exec;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DList<T extends Dyno> extends DNative implements Iterable<T>
{
	private final ArrayList<T> impl;
	
	public DList() {
		this.impl = new ArrayList<T>();
	}
	
	@Override
	public Dyno call(List<Dyno> args) {
		if (args.size() != 1) {
			throw new TypeError("list must be called with one int arg");
		}
		
		Dyno arg = args.get(0);
		
		if (!(arg instanceof DInt)) {
			throw new TypeError("list must be called with one int arg");
		}
		
		int i = ((DInt)arg).value;
		
		if (i < 0 || i >= this.size()) {
			throw new TypeError("list index out of bounds");
		}
		
		return this.get(i);
	}

	public void add(T arg0) {
		this.impl.add(arg0);
	}
	
	public void add(int arg0, T arg1) {
		this.impl.add(arg0, arg1);
	}
	
	public void clear() {
		this.impl.clear();
	}
	
	public T get(int arg0) {
		return this.impl.get(arg0);
	}
	
	public Iterator<T> iterator() {
		return this.impl.iterator();
	}
	
	public void set(int arg0, T arg1) {
		this.impl.set(arg0, arg1);
	}
	
	public int size() {
		return this.impl.size();
	}
}
