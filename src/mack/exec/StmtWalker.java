package mack.exec;

import mack.tree.Assign;
import mack.tree.Block;
import mack.tree.ExprStmt;
import mack.tree.FieldSet;
import mack.tree.If;
import mack.tree.MacroStmt;
import mack.tree.My;
import mack.tree.PlaceStmt;
import mack.tree.Ret;
import mack.tree.Use;
import mack.tree.While;

public interface StmtWalker<T> {
	public T walk(Assign obj);
	public T walk(Block obj);
	public T walk(FieldSet obj);
	public T walk(If obj);
	public T walk(MacroStmt obj);
	public T walk(PlaceStmt obj);
	public T walk(My obj);
	public T walk(Ret obj);
	public T walk(Use obj);
	public T walk(While obj);
	public T walk(ExprStmt obj);
}
