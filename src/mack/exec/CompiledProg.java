package mack.exec;

import mack.tree.Func;

public class CompiledProg
{
	public final Env env;
	public final Func main;
	
	public CompiledProg(Env env, Func main) {
		this.env = env;
		this.main = main;
	}
	
	public Dyno execute() {
		return Executor.Instance.execute(this);
	}
}
