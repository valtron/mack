package mack.exec;

// Doesn't allow changing values of the parent
class ProtectedEnv extends EnvImpl
{
	public ProtectedEnv(Env parent) {
		super(parent);
	}
	
	public void set(String name, Dyno obj) {
		if (this.data.containsKey(name)) {
			this.data.put(name, obj);
		} else {
			throw new CompilationError("cannot set write-protected variable '" + name + "'");
		}
	}
}
