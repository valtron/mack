package mack.exec;

import java.util.ArrayList;
import java.util.List;

import mack.tree.Func;
import mack.tree.MacroSuite;
import mack.tree.Module;
import mack.tree.My;
import mack.tree.Suite;
import mack.tree.Use;

public class CompilerWalker implements SuiteWalker
{
	private final CompilationContext context;
	private final Env env;
	
	public CompilerWalker(CompilationContext context, Env env) {
		this.context = context;
		this.env = env;
	}
	
	@Override
	public void walk(Func obj) {
		obj.body = CompilationUtils.MacroExpand(this.env, obj.body);
	}
	
	@Override
	public void walk(MacroSuite obj) {
		ExecUtils.Execute(this.env, obj.expr);
	}
	
	@Override
	public void walk(Module obj) {
		List<Suite> items = new ArrayList<Suite>();
		
		for (Suite s: obj.items) {
			items.add(s);
		}
		
		CompilationUtils.Compile(this.context, this.env, items);
	}
	
	@Override
	public void walk(My obj) {
		obj.value = CompilationUtils.MacroExpand(this.env, obj.value);
	}
	
	@Override
	public void walk(Use obj) {
		Dyno result = ExecUtils.Execute(this.env, obj.aliased);
		
		if (!(result instanceof Suite)) {
			throw new TypeError("use statement did not evaluate to a module-level item");
		}
		
		obj.resolved = (Suite)result;
	}
}
