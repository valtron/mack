package mack.exec;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class DOutFile extends DNative
{
	private final BufferedWriter writer;
	
	public DOutFile(String filename) throws IOException {
		this.writer = new BufferedWriter(new FileWriter(filename));
	}
	
	public DOutFile(OutputStream out) {
		this.writer = new BufferedWriter(new OutputStreamWriter(out));
	}
	
	public void write(String str) throws IOException {
		this.writer.write(str);
		this.writer.flush();
	}
}
