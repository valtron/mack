package mack.exec;

import java.util.List;

import mack.tree.Suite;

class SuiteCompilation implements Dyno
{
	public final Suite suite;
	private final CompilationContext c;
	private final Env e;
	private Suite compiled;
	
	public SuiteCompilation(CompilationContext c, Env e, Suite suite) {
		this.c = c;
		this.e = e;
		this.suite = suite;
		this.compiled = null;
	}
	
	public Suite getCompiled() {
		if (this.compiled == null) {
			this.suite.accept(new CompilerWalker(this.c, this.e));
			this.compiled = this.suite;
		}
		
		return this.compiled;
	}

	@Override
	public Dyno getField(String name) {
		return this.getCompiled().getField(name);
	}

	@Override
	public void setField(String name, Dyno value) {
		this.getCompiled().setField(name, value);
	}

	@Override
	public Dyno call(List<Dyno> args) {
		return this.getCompiled().call(args);
	}
}
