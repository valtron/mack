package mack.exec;

import java.util.ArrayList;
import java.util.List;

import mack.tree.Assign;
import mack.tree.Block;
import mack.tree.Call;
import mack.tree.Expr;
import mack.tree.ExprStmt;
import mack.tree.FieldGet;
import mack.tree.FieldSet;
import mack.tree.Func;
import mack.tree.If;
import mack.tree.MacroExpr;
import mack.tree.MacroStmt;
import mack.tree.My;
import mack.tree.PlaceExpr;
import mack.tree.PlaceStmt;
import mack.tree.Ret;
import mack.tree.Stmt;
import mack.tree.Suite;
import mack.tree.Use;
import mack.tree.Value;
import mack.tree.Var;
import mack.tree.While;

class ExecWalker implements StmtWalker<Res>, ExprWalker<Res>
{
	private final Env env;
	
	public ExecWalker(Env env) {
		this.env = EnvUtils.New(env);
	}
	
	@Override
	public Res walk(Call obj) {
		Res fun = obj.obj.accept(this);
		List<Dyno> args = new ArrayList<Dyno>();
		
		for (Expr arg: obj.args) {
			args.add(arg.accept(this).value);
		}
		
		if (fun.value instanceof SuiteCompilation) {
			SuiteCompilation sc = (SuiteCompilation)fun.value;
			Suite compiled = sc.getCompiled();
			
			if (compiled instanceof Func) {
				Func f = (Func)compiled;
				TypeError.CheckMinimumArgumentCount(f.args.size(), args);
				Env funcEnv = EnvUtils.New(this.env);
				
				int i = 0;
				while (i < args.size()) {
					funcEnv.declare(f.args.get(i).name.value, args.get(i));
					i += 1;
				}
				
				ExecWalker w = new ExecWalker(funcEnv);
				return f.body.accept(w);
			}
		}
		
		return Res.New(fun.value.call(args));
	}
	
	@Override
	public Res walk(FieldGet obj) {
		Res o = obj.obj.accept(this);
		return Res.New(o.value.getField(obj.field.value));
	}
	
	@Override
	public Res walk(Value obj) {
		return Res.New(obj.value);
	}
	
	@Override
	public Res walk(Var obj) {
		Dyno value = this.env.get(obj.name.value);
		
		if (value == null) {
			throw new ExecError("undefined name '" + obj.name.value + "'");
		}
		
		return Res.New(value);
	}
	
	@Override
	public Res walk(MacroExpr obj) {
		throw ExecError.UnexpandedMacro;
	}
	
	@Override
	public Res walk(Assign obj) {
		Res value = obj.value.accept(this);
		this.env.set(obj.var.name.value, value.value);
		return Res.Void;
	}
	
	@Override
	public Res walk(Block obj) {
		ExecWalker walker = new ExecWalker(EnvUtils.New(this.env));
		
		for (Stmt stmt: obj.stmts) {
			Res r = stmt.accept(walker);
			
			if (r.isRet) {
				return r;
			}
		}
		
		return Res.Void;
	}
	
	@Override
	public Res walk(FieldSet obj) {
		Res o = obj.obj.accept(this);
		Res v = obj.value.accept(this);
		o.value.setField(obj.field.value, v.value);
		return Res.Void;
	}
	
	@Override
	public Res walk(If obj) {
		Res cond = obj.cond.accept(this);
		
		if (ExecUtils.IsTrue(cond.value)) {
			Res r = obj.thenStmt.accept(this);
			if (r.isRet) return r;
		} else if (obj.elseStmt != null) {
			Res r = obj.elseStmt.accept(this);
			if (r.isRet) return r;
		}
		
		return Res.Void;
	}
	
	@Override
	public Res walk(MacroStmt obj) {
		throw ExecError.UnexpandedMacro;
	}
	
	@Override
	public Res walk(My obj) {
		Res value = obj.value.accept(this);
		this.env.declare(obj.varName.value, value.value);
		return Res.Void;
	}
	
	@Override
	public Res walk(Ret obj) {
		if (obj.value == null) {
			return Res.Ret;
		}
		
		Res r = obj.value.accept(this);
		r.isRet = true;
		return r;
	}
	
	@Override
	public Res walk(Use obj) {
		return Res.New(obj.resolved);
	}
	
	@Override
	public Res walk(While obj) {
		Res cond = obj.cond.accept(this);
		while (ExecUtils.IsTrue(cond.value)) {
			Res r = obj.body.accept(this);
			
			if (r.isRet) {
				return r;
			}
			
			cond = obj.cond.accept(this);
		}
		return Res.Void;
	}

	@Override
	public Res walk(ExprStmt obj) {
		obj.expr.accept(this);
		return Res.Void;
	}

	@Override
	public Res walk(PlaceExpr obj) {
		throw ExecError.UnexpandedPlaceholder;
	}

	@Override
	public Res walk(PlaceStmt obj) {
		throw ExecError.UnexpandedPlaceholder;
	}
}
