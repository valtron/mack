package mack.exec;

import java.util.List;

public class TypeError extends Error
{
	private static final long serialVersionUID = -622890432924402509L;
	
	public TypeError(String message) {
		super(message);
	}
	
	public static TypeError HasNotField(String field) {
		throw new TypeError("object has no field '" + field + "'");
	}
	
	public static TypeError SetInvalidType(String field) {
		return new TypeError("tried to set field '" + field + "' with value of incompatible type");
	}
	
	public static TypeError SetNonExistentField(String field) {
		return new TypeError("cannot to set non-existent field '" + field + "'");
	}
	
	public static TypeError NotAFunction() {
		return new TypeError("object is not a function");
	}

	public static TypeError SetReadonlyField(String field) {
		return new TypeError("cannot set readonly field '" + field + "'");
	}

	public static TypeError ExpectedArgumentCount(int given, int expected) {
		return new TypeError("expected '" + expected + "' arguments, given '" + given + "'");
	}
	
	public static TypeError ExpectedMinimumArgumentCount(int given, int expected) {
		return new TypeError("expected at least '" + expected + "' arguments, but got '" + given + "'");
	}
	
	public static void CheckMinimumArgumentCount(int minimum, List<Dyno> args) {
		int given = args.size();
		if (given < minimum) {
			throw TypeError.ExpectedMinimumArgumentCount(given, minimum);
		}
	}
	
	public static void CheckArgumentCount(int expected, List<Dyno> args) {
		int given = args.size();
		
		if (given != expected) {
			throw TypeError.ExpectedArgumentCount(given, expected);
		}
	}
	
	public static void CheckArgumentTypes(List<Dyno> args, Class<?>... types) {
		if (args.size() != types.length) {
			throw TypeError.ExpectedArgumentCount(args.size(), types.length);
		}
		
		for (int i = 0; i < types.length; i++) {
			if (!(args.get(i).getClass() == types[i])) {
				throw TypeError.ArgumentInvalidType(i, types[i].getName());
			}
		}
	}

	public static TypeError ArgumentInvalidType(int i, String type) {
		return new TypeError("argument #" + i + " must be of type '" + type + "'");
	}
}
