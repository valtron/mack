package mack.exec;

import java.util.List;

import mack.tree.Expr;
import mack.tree.Func;
import mack.tree.Stmt;
import mack.tree.Suite;

import mack.exec.ParsedProg;

class CompilationUtils {
	private CompilationUtils() {}
	
	public static CompiledProg Compile(ParsedProg prog) {
		Env root = BuiltIns.GetRootEnv();
		CompilationContext context = new CompilationContext();
		CompilationUtils.Compile(context, root, prog.items);
		
		if (context.compiled == null) {
			throw new CompilationError("main function not specified");
		}
		
		if (context.compiled.main.args.size() != 0) {
			throw new CompilationError("main function must take no args");
		}
		
		return context.compiled;
	}
	
	public static void Compile(CompilationContext context, Env parent, List<Suite> items) {
		Env inner = EnvUtils.New(parent);
		
		for (Suite s: items) {
			String name = s.getName();
			inner.declare(name, new SuiteCompilation(context, inner, s));
		}
		
		for (Suite s: items) {
			String name = s.getName();
			Suite compiled = ((SuiteCompilation)inner.get(name)).getCompiled();
			
			if (compiled instanceof Func && name.equals("main")) {
				context.compiled = new CompiledProg(inner, (Func)compiled);
			}
		}
	}
	
	public static Stmt MacroExpand(Env parent, Stmt obj) {
		MacroExpandWalker v = new MacroExpandWalker(parent);
		return obj.accept(v);
	}
	
	public static Expr MacroExpand(Env parent, Expr obj) {
		MacroExpandWalker v = new MacroExpandWalker(parent);
		return obj.accept(v);
	}
}
