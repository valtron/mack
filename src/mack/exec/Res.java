package mack.exec;

// The result of an execution
public class Res
{
	public boolean isRet;
	public final Dyno value;
	
	private Res(boolean isRet, Dyno value) {
		this.isRet = isRet;
		this.value = value;
	}
	
	public final static Res Void = new Res(false, null);
	
	public final static Res Ret = new Res(true, null);
	
	public final static Res Ret(Dyno value) {
		return new Res(true, value);
	}
	
	public final static Res New(Dyno value) {
		return new Res(false, value);
	}
}
