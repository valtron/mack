package mack.exec;

interface Env {
	public Dyno get(String name);
	public void declare(String name, Dyno obj);
	public void set(String name, Dyno obj);
}
