package mack.exec;

public class Executor
{
	public final static Executor Instance = new Executor();
	
	private Executor() {}
	
	public Dyno execute(CompiledProg p) {
		return ExecUtils.Execute(p.env, p.main.body);
	}
}
