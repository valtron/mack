package mack;

import mack.exec.ParsedProg;
import mack.exec.CompiledProg;

public class Main
{
	private Main() {}
	
	public static void main(String[] args) {
		String file = "files/test1.mc";
		
		ParsedProg pp = ParsedProg.Parse(file);
		CompiledProg cp = pp.compile();
		cp.execute();
	}
}
