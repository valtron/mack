grammar Mack;

options {
	language = Java;
}

@parser::header {
	package mack.parser.impl;
	
	import mack.parser.Converter;
	import mack.exec.ParsedProg;
	import mack.tree.*;
}

@lexer::header {
	package mack.parser.impl;
}

@parser::members {
	private Stmt createAssign(Expr lhs, Expr rhs) {
		if (lhs instanceof Var) {
			return new Assign((Var)lhs, rhs);
		}
		
		if (lhs instanceof FieldGet) {
			FieldGet fg = (FieldGet)lhs;
			return new FieldSet(fg.obj, fg.field, rhs);
		}
		
		this.emitErrorMessage("lhs of assignment is not assignable");
		
		return null;
	}
	
	private MacroSuite createDecorated(Expr e, Suite s) {
		Call c;
		
		if (e instanceof Call) {
			c = (Call)e;
		} else {
			c = new Call(e, new Args());
		}
		
		c.args.add(0, new Value(s));
		
		return new MacroSuite(c);
	}
}

prog returns [ParsedProg node]
@init { node = new ParsedProg(); }:
	(s = suite { node.items.add(s); })*
;

suite returns [Suite node]:
	mo = module { node = mo; } |
	f = func { node = f; } |
	u = use { node = u; } |
	m = my { node = m; } |
	'@' e = callExpr su = suite { node = this.createDecorated(e, su); }
;

module returns [Module node]
@init { node = new Module(); }:
	'module' n = IDENT { node.name = Converter.Wrap(n.getText()); }
	'{' (s = suite { node.items.add(s); })* '}'
;

func returns [Func node]
@init { node = new Func(); }:
	'fun' n = IDENT { node.name = Converter.Wrap(n.getText()); }
	'(' (a = var { node.args.add(a); } (',' a = var { node.args.add(a); })*)? ')'
	b = block { node.body = b; }
;

use returns [Use node]
@init { node = new Use(); }:
	'use' v = IDENT '=' e = expr
	{
		node.alias = Converter.Wrap(v.getText());
		node.aliased = e;
	}
;

block returns [Block node]
@init { node = new Block(); }:
	'{' (s = stmt { node.stmts.add(s); })* '}'
;

stmt returns [Stmt node]:
	a = assign { node = a; } |
	r = ret { node = r; } |
	m = my { node = m; } |
	i = ifElse { node = i; } |
	b = block { node = b; } |
	w = whileLoop { node = w; } |
	'@' e = callExpr ';' { node = new MacroStmt(e); } |
	'%' e = callExpr ';' { node = new PlaceStmt(e); } |
	u = use { node = u; }
;

assign returns [Stmt node]:
	lhs = callExpr
	(
		'=' rhs = expr { node = this.createAssign(lhs, rhs); }
		| { node = new ExprStmt(lhs); }
	)
	';'
;

ret returns [Ret node]
@init { node = new Ret(); }:
	'ret' (e = expr { node.value = e; })? ';'
;

my returns [My node]
@init { node = new My(); }:
	'my' v = IDENT { node.varName = Converter.Wrap(v.getText()); }
	'=' e = expr { node.value = e; } ';'
;

ifElse returns [If node]
@init { node = new If(); }:
	'if' c = expr b = block ('else' e = stmt)?
	{
		node.cond = c;
		node.thenStmt = b;
		node.elseStmt = e;
	}
;

whileLoop returns [While node]
@init { node = new While(); }:
	'while' c = expr b = block
	{
		node.cond = c;
		node.body = b;
	}
;

expr returns [Expr node]:
	c = callExpr { node = c; } |
	'@' e = callExpr { node = new MacroExpr(e); } |
	'%' e = callExpr { node = new PlaceExpr(e); }
;
callExpr returns [Expr node]:
	l = baseExpr { node = l; } (
		a = args { node = new Call(node, a); } |
		'.' f = IDENT { node = new FieldGet(node, Converter.Wrap(f.getText())); }
	)*
;
baseExpr returns [Expr node]:
	vr = var { node = vr; } |
	vl = value { node = vl; }
;

args returns [Args node]
@init { node = new Args(); }:
	'(' (e = expr { node.add(e); } (',' e = expr { node.add(e); })*)? ')'
;

var returns [Var node]: t = IDENT { node = new Var(Converter.Wrap(t.getText())); };
value returns [Value node]:
	t = INT { node = new Value(Converter.Int(t.getText())); } |
	t = STRING { node = new Value(Converter.Unquote(t.getText())); } |
	qs = quotedStmt { node = new Value(qs); } |
	qe = quotedExpr { node = new Value(qe); }
;

quotedStmt returns [Block node]
@init { node = new Block(); }:
	'{:' (s = stmt { node.stmts.add(s); })* ':}'
;

quotedExpr returns [Expr node]: '{=' e = expr '=}' { node = e; };

fragment ALPHA: ('a' .. 'z' | 'A' .. 'Z' | '_');
fragment DIGIT: '0' .. '9';
fragment ALNUM: ALPHA | DIGIT;

INT: ('-'|'+')? DIGIT+;
STRING: '"' (~('"' | '\\') | '\\' .)* '"';
IDENT: ALPHA ALNUM* '\''?;

WS: (' '|'\t'|'\r'|'\n'|'\f')+ { $channel = HIDDEN; };
C_ML: '/*' (options {greedy=false;} : .)* '*/' { $channel = HIDDEN; };
C_SL: '//' ~('\n'|'\r')* ('\r'? '\n')? { $channel = HIDDEN; };
