package mack.parser;

import mack.exec.DInt;
import mack.exec.DString;

public class Converter
{
	private Converter() {}
	
	public static DInt Int(String str) {
		return DInt.Wrap(Integer.parseInt(str));
	}
	
	public static DString Unquote(String str) {
		str = str.substring(1, str.length() - 1);
		str = str.replaceAll("\\\"", "\"");
		str = str.replaceAll("\\n", "\n");
		str = str.replaceAll("\\t", "\t");
		str = str.replaceAll("\\\\", "\\");
		return new DString(str);
	}
	
	public static DString Wrap(String str) {
		return new DString(str);
	}
}
