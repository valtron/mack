package mack.parser;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.TokenStream;

import mack.parser.impl.MackLexer;
import mack.parser.impl.MackParser;

import mack.exec.ParsedProg;

public class Parser {
	public final static Parser Instance = new Parser();
	
	private Parser() {}
	
	public ParsedProg parse(String file) {
		try {
			MackLexer lexer = new MackLexer(new ANTLRFileStream(file));
			TokenStream stream = new CommonTokenStream(lexer);
			MackParser impl = new MackParser(stream);
			return impl.prog();
		} catch (Exception ex) {
			System.err.println(ex.toString());
		}
		
		return null;
	}
}
