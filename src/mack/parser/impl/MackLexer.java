// $ANTLR 3.4 C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g 2012-07-17 21:10:21

	package mack.parser.impl;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MackLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int ALNUM=4;
    public static final int ALPHA=5;
    public static final int C_ML=6;
    public static final int C_SL=7;
    public static final int DIGIT=8;
    public static final int IDENT=9;
    public static final int INT=10;
    public static final int STRING=11;
    public static final int WS=12;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public MackLexer() {} 
    public MackLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public MackLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:11:7: ( '%' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:11:9: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:12:7: ( '(' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:12:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:13:7: ( ')' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:13:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:14:7: ( ',' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:14:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:15:7: ( '.' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:15:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:16:7: ( ':}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:16:9: ':}'
            {
            match(":}"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:17:7: ( ';' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:17:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:18:7: ( '=' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:18:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:19:7: ( '=}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:19:9: '=}'
            {
            match("=}"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:20:7: ( '@' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:20:9: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:21:7: ( 'else' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:21:9: 'else'
            {
            match("else"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:22:7: ( 'fun' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:22:9: 'fun'
            {
            match("fun"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:23:7: ( 'if' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:23:9: 'if'
            {
            match("if"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:24:7: ( 'module' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:24:9: 'module'
            {
            match("module"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:25:7: ( 'my' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:25:9: 'my'
            {
            match("my"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:26:7: ( 'ret' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:26:9: 'ret'
            {
            match("ret"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:27:7: ( 'use' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:27:9: 'use'
            {
            match("use"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:28:7: ( 'while' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:28:9: 'while'
            {
            match("while"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:29:7: ( '{' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:29:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:30:7: ( '{:' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:30:9: '{:'
            {
            match("{:"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:31:7: ( '{=' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:31:9: '{='
            {
            match("{="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:32:7: ( '}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:32:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "ALPHA"
    public final void mALPHA() throws RecognitionException {
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:177:15: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ALPHA"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:178:15: ( '0' .. '9' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "ALNUM"
    public final void mALNUM() throws RecognitionException {
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:179:15: ( ALPHA | DIGIT )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ALNUM"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:181:4: ( ( '-' | '+' )? ( DIGIT )+ )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:181:6: ( '-' | '+' )? ( DIGIT )+
            {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:181:6: ( '-' | '+' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='+'||LA1_0=='-') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:181:17: ( DIGIT )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:182:7: ( '\"' (~ ( '\"' | '\\\\' ) | '\\\\' . )* '\"' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:182:9: '\"' (~ ( '\"' | '\\\\' ) | '\\\\' . )* '\"'
            {
            match('\"'); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:182:13: (~ ( '\"' | '\\\\' ) | '\\\\' . )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= '\u0000' && LA3_0 <= '!')||(LA3_0 >= '#' && LA3_0 <= '[')||(LA3_0 >= ']' && LA3_0 <= '\uFFFF')) ) {
                    alt3=1;
                }
                else if ( (LA3_0=='\\') ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:182:14: ~ ( '\"' | '\\\\' )
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:182:30: '\\\\' .
            	    {
            	    match('\\'); 

            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "IDENT"
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:183:6: ( ALPHA ( ALNUM )* ( '\\'' )? )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:183:8: ALPHA ( ALNUM )* ( '\\'' )?
            {
            mALPHA(); 


            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:183:14: ( ALNUM )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'Z')||LA4_0=='_'||(LA4_0 >= 'a' && LA4_0 <= 'z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:183:21: ( '\\'' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='\'') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:183:21: '\\''
                    {
                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:185:3: ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\f' )+ )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:185:5: ( ' ' | '\\t' | '\\r' | '\\n' | '\\f' )+
            {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:185:5: ( ' ' | '\\t' | '\\r' | '\\n' | '\\f' )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0 >= '\t' && LA6_0 <= '\n')||(LA6_0 >= '\f' && LA6_0 <= '\r')||LA6_0==' ') ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            	    {
            	    if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


             _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "C_ML"
    public final void mC_ML() throws RecognitionException {
        try {
            int _type = C_ML;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:186:5: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:186:7: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 



            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:186:12: ( options {greedy=false; } : . )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0=='*') ) {
                    int LA7_1 = input.LA(2);

                    if ( (LA7_1=='/') ) {
                        alt7=2;
                    }
                    else if ( ((LA7_1 >= '\u0000' && LA7_1 <= '.')||(LA7_1 >= '0' && LA7_1 <= '\uFFFF')) ) {
                        alt7=1;
                    }


                }
                else if ( ((LA7_0 >= '\u0000' && LA7_0 <= ')')||(LA7_0 >= '+' && LA7_0 <= '\uFFFF')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:186:39: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            match("*/"); 



             _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "C_ML"

    // $ANTLR start "C_SL"
    public final void mC_SL() throws RecognitionException {
        try {
            int _type = C_SL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( ( '\\r' )? '\\n' )? )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:7: '//' (~ ( '\\n' | '\\r' ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 



            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:12: (~ ( '\\n' | '\\r' ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0 >= '\u0000' && LA8_0 <= '\t')||(LA8_0 >= '\u000B' && LA8_0 <= '\f')||(LA8_0 >= '\u000E' && LA8_0 <= '\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:26: ( ( '\\r' )? '\\n' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\n'||LA10_0=='\r') ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:27: ( '\\r' )? '\\n'
                    {
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:27: ( '\\r' )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0=='\r') ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:187:27: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }


                    match('\n'); 

                    }
                    break;

            }


             _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "C_SL"

    public void mTokens() throws RecognitionException {
        // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | INT | STRING | IDENT | WS | C_ML | C_SL )
        int alt11=28;
        switch ( input.LA(1) ) {
        case '%':
            {
            alt11=1;
            }
            break;
        case '(':
            {
            alt11=2;
            }
            break;
        case ')':
            {
            alt11=3;
            }
            break;
        case ',':
            {
            alt11=4;
            }
            break;
        case '.':
            {
            alt11=5;
            }
            break;
        case ':':
            {
            alt11=6;
            }
            break;
        case ';':
            {
            alt11=7;
            }
            break;
        case '=':
            {
            int LA11_8 = input.LA(2);

            if ( (LA11_8=='}') ) {
                alt11=9;
            }
            else {
                alt11=8;
            }
            }
            break;
        case '@':
            {
            alt11=10;
            }
            break;
        case 'e':
            {
            int LA11_10 = input.LA(2);

            if ( (LA11_10=='l') ) {
                int LA11_26 = input.LA(3);

                if ( (LA11_26=='s') ) {
                    int LA11_39 = input.LA(4);

                    if ( (LA11_39=='e') ) {
                        int LA11_47 = input.LA(5);

                        if ( (LA11_47=='\''||(LA11_47 >= '0' && LA11_47 <= '9')||(LA11_47 >= 'A' && LA11_47 <= 'Z')||LA11_47=='_'||(LA11_47 >= 'a' && LA11_47 <= 'z')) ) {
                            alt11=25;
                        }
                        else {
                            alt11=11;
                        }
                    }
                    else {
                        alt11=25;
                    }
                }
                else {
                    alt11=25;
                }
            }
            else {
                alt11=25;
            }
            }
            break;
        case 'f':
            {
            int LA11_11 = input.LA(2);

            if ( (LA11_11=='u') ) {
                int LA11_27 = input.LA(3);

                if ( (LA11_27=='n') ) {
                    int LA11_40 = input.LA(4);

                    if ( (LA11_40=='\''||(LA11_40 >= '0' && LA11_40 <= '9')||(LA11_40 >= 'A' && LA11_40 <= 'Z')||LA11_40=='_'||(LA11_40 >= 'a' && LA11_40 <= 'z')) ) {
                        alt11=25;
                    }
                    else {
                        alt11=12;
                    }
                }
                else {
                    alt11=25;
                }
            }
            else {
                alt11=25;
            }
            }
            break;
        case 'i':
            {
            int LA11_12 = input.LA(2);

            if ( (LA11_12=='f') ) {
                int LA11_28 = input.LA(3);

                if ( (LA11_28=='\''||(LA11_28 >= '0' && LA11_28 <= '9')||(LA11_28 >= 'A' && LA11_28 <= 'Z')||LA11_28=='_'||(LA11_28 >= 'a' && LA11_28 <= 'z')) ) {
                    alt11=25;
                }
                else {
                    alt11=13;
                }
            }
            else {
                alt11=25;
            }
            }
            break;
        case 'm':
            {
            switch ( input.LA(2) ) {
            case 'o':
                {
                int LA11_29 = input.LA(3);

                if ( (LA11_29=='d') ) {
                    int LA11_42 = input.LA(4);

                    if ( (LA11_42=='u') ) {
                        int LA11_49 = input.LA(5);

                        if ( (LA11_49=='l') ) {
                            int LA11_54 = input.LA(6);

                            if ( (LA11_54=='e') ) {
                                int LA11_56 = input.LA(7);

                                if ( (LA11_56=='\''||(LA11_56 >= '0' && LA11_56 <= '9')||(LA11_56 >= 'A' && LA11_56 <= 'Z')||LA11_56=='_'||(LA11_56 >= 'a' && LA11_56 <= 'z')) ) {
                                    alt11=25;
                                }
                                else {
                                    alt11=14;
                                }
                            }
                            else {
                                alt11=25;
                            }
                        }
                        else {
                            alt11=25;
                        }
                    }
                    else {
                        alt11=25;
                    }
                }
                else {
                    alt11=25;
                }
                }
                break;
            case 'y':
                {
                int LA11_30 = input.LA(3);

                if ( (LA11_30=='\''||(LA11_30 >= '0' && LA11_30 <= '9')||(LA11_30 >= 'A' && LA11_30 <= 'Z')||LA11_30=='_'||(LA11_30 >= 'a' && LA11_30 <= 'z')) ) {
                    alt11=25;
                }
                else {
                    alt11=15;
                }
                }
                break;
            default:
                alt11=25;
            }

            }
            break;
        case 'r':
            {
            int LA11_14 = input.LA(2);

            if ( (LA11_14=='e') ) {
                int LA11_31 = input.LA(3);

                if ( (LA11_31=='t') ) {
                    int LA11_44 = input.LA(4);

                    if ( (LA11_44=='\''||(LA11_44 >= '0' && LA11_44 <= '9')||(LA11_44 >= 'A' && LA11_44 <= 'Z')||LA11_44=='_'||(LA11_44 >= 'a' && LA11_44 <= 'z')) ) {
                        alt11=25;
                    }
                    else {
                        alt11=16;
                    }
                }
                else {
                    alt11=25;
                }
            }
            else {
                alt11=25;
            }
            }
            break;
        case 'u':
            {
            int LA11_15 = input.LA(2);

            if ( (LA11_15=='s') ) {
                int LA11_32 = input.LA(3);

                if ( (LA11_32=='e') ) {
                    int LA11_45 = input.LA(4);

                    if ( (LA11_45=='\''||(LA11_45 >= '0' && LA11_45 <= '9')||(LA11_45 >= 'A' && LA11_45 <= 'Z')||LA11_45=='_'||(LA11_45 >= 'a' && LA11_45 <= 'z')) ) {
                        alt11=25;
                    }
                    else {
                        alt11=17;
                    }
                }
                else {
                    alt11=25;
                }
            }
            else {
                alt11=25;
            }
            }
            break;
        case 'w':
            {
            int LA11_16 = input.LA(2);

            if ( (LA11_16=='h') ) {
                int LA11_33 = input.LA(3);

                if ( (LA11_33=='i') ) {
                    int LA11_46 = input.LA(4);

                    if ( (LA11_46=='l') ) {
                        int LA11_52 = input.LA(5);

                        if ( (LA11_52=='e') ) {
                            int LA11_55 = input.LA(6);

                            if ( (LA11_55=='\''||(LA11_55 >= '0' && LA11_55 <= '9')||(LA11_55 >= 'A' && LA11_55 <= 'Z')||LA11_55=='_'||(LA11_55 >= 'a' && LA11_55 <= 'z')) ) {
                                alt11=25;
                            }
                            else {
                                alt11=18;
                            }
                        }
                        else {
                            alt11=25;
                        }
                    }
                    else {
                        alt11=25;
                    }
                }
                else {
                    alt11=25;
                }
            }
            else {
                alt11=25;
            }
            }
            break;
        case '{':
            {
            switch ( input.LA(2) ) {
            case ':':
                {
                alt11=20;
                }
                break;
            case '=':
                {
                alt11=21;
                }
                break;
            default:
                alt11=19;
            }

            }
            break;
        case '}':
            {
            alt11=22;
            }
            break;
        case '+':
        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
            alt11=23;
            }
            break;
        case '\"':
            {
            alt11=24;
            }
            break;
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'g':
        case 'h':
        case 'j':
        case 'k':
        case 'l':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 's':
        case 't':
        case 'v':
        case 'x':
        case 'y':
        case 'z':
            {
            alt11=25;
            }
            break;
        case '\t':
        case '\n':
        case '\f':
        case '\r':
        case ' ':
            {
            alt11=26;
            }
            break;
        case '/':
            {
            int LA11_23 = input.LA(2);

            if ( (LA11_23=='*') ) {
                alt11=27;
            }
            else if ( (LA11_23=='/') ) {
                alt11=28;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 23, input);

                throw nvae;

            }
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 11, 0, input);

            throw nvae;

        }

        switch (alt11) {
            case 1 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:10: T__13
                {
                mT__13(); 


                }
                break;
            case 2 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:16: T__14
                {
                mT__14(); 


                }
                break;
            case 3 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:22: T__15
                {
                mT__15(); 


                }
                break;
            case 4 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:28: T__16
                {
                mT__16(); 


                }
                break;
            case 5 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:34: T__17
                {
                mT__17(); 


                }
                break;
            case 6 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:40: T__18
                {
                mT__18(); 


                }
                break;
            case 7 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:46: T__19
                {
                mT__19(); 


                }
                break;
            case 8 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:52: T__20
                {
                mT__20(); 


                }
                break;
            case 9 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:58: T__21
                {
                mT__21(); 


                }
                break;
            case 10 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:64: T__22
                {
                mT__22(); 


                }
                break;
            case 11 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:70: T__23
                {
                mT__23(); 


                }
                break;
            case 12 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:76: T__24
                {
                mT__24(); 


                }
                break;
            case 13 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:82: T__25
                {
                mT__25(); 


                }
                break;
            case 14 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:88: T__26
                {
                mT__26(); 


                }
                break;
            case 15 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:94: T__27
                {
                mT__27(); 


                }
                break;
            case 16 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:100: T__28
                {
                mT__28(); 


                }
                break;
            case 17 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:106: T__29
                {
                mT__29(); 


                }
                break;
            case 18 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:112: T__30
                {
                mT__30(); 


                }
                break;
            case 19 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:118: T__31
                {
                mT__31(); 


                }
                break;
            case 20 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:124: T__32
                {
                mT__32(); 


                }
                break;
            case 21 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:130: T__33
                {
                mT__33(); 


                }
                break;
            case 22 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:136: T__34
                {
                mT__34(); 


                }
                break;
            case 23 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:142: INT
                {
                mINT(); 


                }
                break;
            case 24 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:146: STRING
                {
                mSTRING(); 


                }
                break;
            case 25 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:153: IDENT
                {
                mIDENT(); 


                }
                break;
            case 26 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:159: WS
                {
                mWS(); 


                }
                break;
            case 27 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:162: C_ML
                {
                mC_ML(); 


                }
                break;
            case 28 :
                // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:1:167: C_SL
                {
                mC_SL(); 


                }
                break;

        }

    }


 

}