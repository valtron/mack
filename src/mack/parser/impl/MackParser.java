// $ANTLR 3.4 C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g 2012-07-17 21:10:21

	package mack.parser.impl;
	
	import mack.parser.Converter;
	import mack.exec.ParsedProg;
	import mack.tree.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MackParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ALNUM", "ALPHA", "C_ML", "C_SL", "DIGIT", "IDENT", "INT", "STRING", "WS", "'%'", "'('", "')'", "','", "'.'", "':}'", "';'", "'='", "'=}'", "'@'", "'else'", "'fun'", "'if'", "'module'", "'my'", "'ret'", "'use'", "'while'", "'{'", "'{:'", "'{='", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int ALNUM=4;
    public static final int ALPHA=5;
    public static final int C_ML=6;
    public static final int C_SL=7;
    public static final int DIGIT=8;
    public static final int IDENT=9;
    public static final int INT=10;
    public static final int STRING=11;
    public static final int WS=12;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public MackParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public MackParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return MackParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g"; }


    	private Stmt createAssign(Expr lhs, Expr rhs) {
    		if (lhs instanceof Var) {
    			return new Assign((Var)lhs, rhs);
    		}
    		
    		if (lhs instanceof FieldGet) {
    			FieldGet fg = (FieldGet)lhs;
    			return new FieldSet(fg.obj, fg.field, rhs);
    		}
    		
    		this.emitErrorMessage("lhs of assignment is not assignable");
    		
    		return null;
    	}
    	
    	private MacroSuite createDecorated(Expr e, Suite s) {
    		Call c;
    		
    		if (e instanceof Call) {
    			c = (Call)e;
    		} else {
    			c = new Call(e, new Args());
    		}
    		
    		c.args.add(0, new Value(s));
    		
    		return new MacroSuite(c);
    	}



    // $ANTLR start "prog"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:50:1: prog returns [ParsedProg node] : (s= suite )* ;
    public final ParsedProg prog() throws RecognitionException {
        ParsedProg node = null;


        Suite s =null;


         node = new ParsedProg(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:51:35: ( (s= suite )* )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:52:2: (s= suite )*
            {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:52:2: (s= suite )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==22||LA1_0==24||(LA1_0 >= 26 && LA1_0 <= 27)||LA1_0==29) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:52:3: s= suite
            	    {
            	    pushFollow(FOLLOW_suite_in_prog64);
            	    s=suite();

            	    state._fsp--;


            	     node.items.add(s); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "prog"



    // $ANTLR start "suite"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:55:1: suite returns [Suite node] : (mo= module |f= func |u= use |m= my | '@' e= callExpr su= suite );
    public final Suite suite() throws RecognitionException {
        Suite node = null;


        Module mo =null;

        Func f =null;

        Use u =null;

        My m =null;

        Expr e =null;

        Suite su =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:55:27: (mo= module |f= func |u= use |m= my | '@' e= callExpr su= suite )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt2=1;
                }
                break;
            case 24:
                {
                alt2=2;
                }
                break;
            case 29:
                {
                alt2=3;
                }
                break;
            case 27:
                {
                alt2=4;
                }
                break;
            case 22:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:56:2: mo= module
                    {
                    pushFollow(FOLLOW_module_in_suite85);
                    mo=module();

                    state._fsp--;


                     node = mo; 

                    }
                    break;
                case 2 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:57:2: f= func
                    {
                    pushFollow(FOLLOW_func_in_suite96);
                    f=func();

                    state._fsp--;


                     node = f; 

                    }
                    break;
                case 3 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:58:2: u= use
                    {
                    pushFollow(FOLLOW_use_in_suite107);
                    u=use();

                    state._fsp--;


                     node = u; 

                    }
                    break;
                case 4 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:59:2: m= my
                    {
                    pushFollow(FOLLOW_my_in_suite118);
                    m=my();

                    state._fsp--;


                     node = m; 

                    }
                    break;
                case 5 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:60:2: '@' e= callExpr su= suite
                    {
                    match(input,22,FOLLOW_22_in_suite125); 

                    pushFollow(FOLLOW_callExpr_in_suite131);
                    e=callExpr();

                    state._fsp--;


                    pushFollow(FOLLOW_suite_in_suite137);
                    su=suite();

                    state._fsp--;


                     node = this.createDecorated(e, su); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "suite"



    // $ANTLR start "module"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:63:1: module returns [Module node] : 'module' n= IDENT '{' (s= suite )* '}' ;
    public final Module module() throws RecognitionException {
        Module node = null;


        Token n=null;
        Suite s =null;


         node = new Module(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:64:31: ( 'module' n= IDENT '{' (s= suite )* '}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:65:2: 'module' n= IDENT '{' (s= suite )* '}'
            {
            match(input,26,FOLLOW_26_in_module157); 

            n=(Token)match(input,IDENT,FOLLOW_IDENT_in_module163); 

             node.name = Converter.Wrap(n.getText()); 

            match(input,31,FOLLOW_31_in_module168); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:66:6: (s= suite )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==22||LA3_0==24||(LA3_0 >= 26 && LA3_0 <= 27)||LA3_0==29) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:66:7: s= suite
            	    {
            	    pushFollow(FOLLOW_suite_in_module175);
            	    s=suite();

            	    state._fsp--;


            	     node.items.add(s); 

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            match(input,34,FOLLOW_34_in_module181); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "module"



    // $ANTLR start "func"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:69:1: func returns [Func node] : 'fun' n= IDENT '(' (a= var ( ',' a= var )* )? ')' b= block ;
    public final Func func() throws RecognitionException {
        Func node = null;


        Token n=null;
        Var a =null;

        Block b =null;


         node = new Func(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:70:29: ( 'fun' n= IDENT '(' (a= var ( ',' a= var )* )? ')' b= block )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:71:2: 'fun' n= IDENT '(' (a= var ( ',' a= var )* )? ')' b= block
            {
            match(input,24,FOLLOW_24_in_func199); 

            n=(Token)match(input,IDENT,FOLLOW_IDENT_in_func205); 

             node.name = Converter.Wrap(n.getText()); 

            match(input,14,FOLLOW_14_in_func210); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:72:6: (a= var ( ',' a= var )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==IDENT) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:72:7: a= var ( ',' a= var )*
                    {
                    pushFollow(FOLLOW_var_in_func217);
                    a=var();

                    state._fsp--;


                     node.args.add(a); 

                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:72:37: ( ',' a= var )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==16) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:72:38: ',' a= var
                    	    {
                    	    match(input,16,FOLLOW_16_in_func222); 

                    	    pushFollow(FOLLOW_var_in_func228);
                    	    a=var();

                    	    state._fsp--;


                    	     node.args.add(a); 

                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }


            match(input,15,FOLLOW_15_in_func236); 

            pushFollow(FOLLOW_block_in_func243);
            b=block();

            state._fsp--;


             node.body = b; 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "func"



    // $ANTLR start "use"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:76:1: use returns [Use node] : 'use' v= IDENT '=' e= expr ;
    public final Use use() throws RecognitionException {
        Use node = null;


        Token v=null;
        Expr e =null;


         node = new Use(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:77:28: ( 'use' v= IDENT '=' e= expr )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:78:2: 'use' v= IDENT '=' e= expr
            {
            match(input,29,FOLLOW_29_in_use263); 

            v=(Token)match(input,IDENT,FOLLOW_IDENT_in_use269); 

            match(input,20,FOLLOW_20_in_use271); 

            pushFollow(FOLLOW_expr_in_use277);
            e=expr();

            state._fsp--;



            		node.alias = Converter.Wrap(v.getText());
            		node.aliased = e;
            	

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "use"



    // $ANTLR start "block"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:85:1: block returns [Block node] : '{' (s= stmt )* '}' ;
    public final Block block() throws RecognitionException {
        Block node = null;


        Stmt s =null;


         node = new Block(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:86:30: ( '{' (s= stmt )* '}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:87:2: '{' (s= stmt )* '}'
            {
            match(input,31,FOLLOW_31_in_block298); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:87:6: (s= stmt )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0 >= IDENT && LA6_0 <= STRING)||LA6_0==13||LA6_0==22||LA6_0==25||(LA6_0 >= 27 && LA6_0 <= 33)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:87:7: s= stmt
            	    {
            	    pushFollow(FOLLOW_stmt_in_block305);
            	    s=stmt();

            	    state._fsp--;


            	     node.stmts.add(s); 

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            match(input,34,FOLLOW_34_in_block311); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "block"



    // $ANTLR start "stmt"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:90:1: stmt returns [Stmt node] : (a= assign |r= ret |m= my |i= ifElse |b= block |w= whileLoop | '@' e= callExpr ';' | '%' e= callExpr ';' |u= use );
    public final Stmt stmt() throws RecognitionException {
        Stmt node = null;


        Stmt a =null;

        Ret r =null;

        My m =null;

        If i =null;

        Block b =null;

        While w =null;

        Expr e =null;

        Use u =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:90:25: (a= assign |r= ret |m= my |i= ifElse |b= block |w= whileLoop | '@' e= callExpr ';' | '%' e= callExpr ';' |u= use )
            int alt7=9;
            switch ( input.LA(1) ) {
            case IDENT:
            case INT:
            case STRING:
            case 32:
            case 33:
                {
                alt7=1;
                }
                break;
            case 28:
                {
                alt7=2;
                }
                break;
            case 27:
                {
                alt7=3;
                }
                break;
            case 25:
                {
                alt7=4;
                }
                break;
            case 31:
                {
                alt7=5;
                }
                break;
            case 30:
                {
                alt7=6;
                }
                break;
            case 22:
                {
                alt7=7;
                }
                break;
            case 13:
                {
                alt7=8;
                }
                break;
            case 29:
                {
                alt7=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;

            }

            switch (alt7) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:91:2: a= assign
                    {
                    pushFollow(FOLLOW_assign_in_stmt328);
                    a=assign();

                    state._fsp--;


                     node = a; 

                    }
                    break;
                case 2 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:92:2: r= ret
                    {
                    pushFollow(FOLLOW_ret_in_stmt339);
                    r=ret();

                    state._fsp--;


                     node = r; 

                    }
                    break;
                case 3 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:93:2: m= my
                    {
                    pushFollow(FOLLOW_my_in_stmt350);
                    m=my();

                    state._fsp--;


                     node = m; 

                    }
                    break;
                case 4 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:94:2: i= ifElse
                    {
                    pushFollow(FOLLOW_ifElse_in_stmt361);
                    i=ifElse();

                    state._fsp--;


                     node = i; 

                    }
                    break;
                case 5 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:95:2: b= block
                    {
                    pushFollow(FOLLOW_block_in_stmt372);
                    b=block();

                    state._fsp--;


                     node = b; 

                    }
                    break;
                case 6 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:96:2: w= whileLoop
                    {
                    pushFollow(FOLLOW_whileLoop_in_stmt383);
                    w=whileLoop();

                    state._fsp--;


                     node = w; 

                    }
                    break;
                case 7 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:97:2: '@' e= callExpr ';'
                    {
                    match(input,22,FOLLOW_22_in_stmt390); 

                    pushFollow(FOLLOW_callExpr_in_stmt396);
                    e=callExpr();

                    state._fsp--;


                    match(input,19,FOLLOW_19_in_stmt398); 

                     node = new MacroStmt(e); 

                    }
                    break;
                case 8 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:98:2: '%' e= callExpr ';'
                    {
                    match(input,13,FOLLOW_13_in_stmt405); 

                    pushFollow(FOLLOW_callExpr_in_stmt411);
                    e=callExpr();

                    state._fsp--;


                    match(input,19,FOLLOW_19_in_stmt413); 

                     node = new PlaceStmt(e); 

                    }
                    break;
                case 9 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:99:2: u= use
                    {
                    pushFollow(FOLLOW_use_in_stmt424);
                    u=use();

                    state._fsp--;


                     node = u; 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "stmt"



    // $ANTLR start "assign"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:102:1: assign returns [Stmt node] : lhs= callExpr ( '=' rhs= expr |) ';' ;
    public final Stmt assign() throws RecognitionException {
        Stmt node = null;


        Expr lhs =null;

        Expr rhs =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:102:27: (lhs= callExpr ( '=' rhs= expr |) ';' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:103:2: lhs= callExpr ( '=' rhs= expr |) ';'
            {
            pushFollow(FOLLOW_callExpr_in_assign443);
            lhs=callExpr();

            state._fsp--;


            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:104:2: ( '=' rhs= expr |)
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==20) ) {
                alt8=1;
            }
            else if ( (LA8_0==19) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:105:3: '=' rhs= expr
                    {
                    match(input,20,FOLLOW_20_in_assign450); 

                    pushFollow(FOLLOW_expr_in_assign456);
                    rhs=expr();

                    state._fsp--;


                     node = this.createAssign(lhs, rhs); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:106:5: 
                    {
                     node = new ExprStmt(lhs); 

                    }
                    break;

            }


            match(input,19,FOLLOW_19_in_assign470); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "assign"



    // $ANTLR start "ret"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:111:1: ret returns [Ret node] : 'ret' (e= expr )? ';' ;
    public final Ret ret() throws RecognitionException {
        Ret node = null;


        Expr e =null;


         node = new Ret(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:112:28: ( 'ret' (e= expr )? ';' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:113:2: 'ret' (e= expr )? ';'
            {
            match(input,28,FOLLOW_28_in_ret488); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:113:8: (e= expr )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0 >= IDENT && LA9_0 <= STRING)||LA9_0==13||LA9_0==22||(LA9_0 >= 32 && LA9_0 <= 33)) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:113:9: e= expr
                    {
                    pushFollow(FOLLOW_expr_in_ret495);
                    e=expr();

                    state._fsp--;


                     node.value = e; 

                    }
                    break;

            }


            match(input,19,FOLLOW_19_in_ret501); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "ret"



    // $ANTLR start "my"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:116:1: my returns [My node] : 'my' v= IDENT '=' e= expr ';' ;
    public final My my() throws RecognitionException {
        My node = null;


        Token v=null;
        Expr e =null;


         node = new My(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:117:27: ( 'my' v= IDENT '=' e= expr ';' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:118:2: 'my' v= IDENT '=' e= expr ';'
            {
            match(input,27,FOLLOW_27_in_my519); 

            v=(Token)match(input,IDENT,FOLLOW_IDENT_in_my525); 

             node.varName = Converter.Wrap(v.getText()); 

            match(input,20,FOLLOW_20_in_my530); 

            pushFollow(FOLLOW_expr_in_my536);
            e=expr();

            state._fsp--;


             node.value = e; 

            match(input,19,FOLLOW_19_in_my540); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "my"



    // $ANTLR start "ifElse"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:122:1: ifElse returns [If node] : 'if' c= expr b= block ( 'else' e= stmt )? ;
    public final If ifElse() throws RecognitionException {
        If node = null;


        Expr c =null;

        Block b =null;

        Stmt e =null;


         node = new If(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:123:27: ( 'if' c= expr b= block ( 'else' e= stmt )? )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:124:2: 'if' c= expr b= block ( 'else' e= stmt )?
            {
            match(input,25,FOLLOW_25_in_ifElse558); 

            pushFollow(FOLLOW_expr_in_ifElse564);
            c=expr();

            state._fsp--;


            pushFollow(FOLLOW_block_in_ifElse570);
            b=block();

            state._fsp--;


            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:124:26: ( 'else' e= stmt )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==23) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:124:27: 'else' e= stmt
                    {
                    match(input,23,FOLLOW_23_in_ifElse573); 

                    pushFollow(FOLLOW_stmt_in_ifElse579);
                    e=stmt();

                    state._fsp--;


                    }
                    break;

            }



            		node.cond = c;
            		node.thenStmt = b;
            		node.elseStmt = e;
            	

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "ifElse"



    // $ANTLR start "whileLoop"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:132:1: whileLoop returns [While node] : 'while' c= expr b= block ;
    public final While whileLoop() throws RecognitionException {
        While node = null;


        Expr c =null;

        Block b =null;


         node = new While(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:133:30: ( 'while' c= expr b= block )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:134:2: 'while' c= expr b= block
            {
            match(input,30,FOLLOW_30_in_whileLoop602); 

            pushFollow(FOLLOW_expr_in_whileLoop608);
            c=expr();

            state._fsp--;


            pushFollow(FOLLOW_block_in_whileLoop614);
            b=block();

            state._fsp--;



            		node.cond = c;
            		node.body = b;
            	

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "whileLoop"



    // $ANTLR start "expr"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:141:1: expr returns [Expr node] : (c= callExpr | '@' e= callExpr | '%' e= callExpr );
    public final Expr expr() throws RecognitionException {
        Expr node = null;


        Expr c =null;

        Expr e =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:141:25: (c= callExpr | '@' e= callExpr | '%' e= callExpr )
            int alt11=3;
            switch ( input.LA(1) ) {
            case IDENT:
            case INT:
            case STRING:
            case 32:
            case 33:
                {
                alt11=1;
                }
                break;
            case 22:
                {
                alt11=2;
                }
                break;
            case 13:
                {
                alt11=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }

            switch (alt11) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:142:2: c= callExpr
                    {
                    pushFollow(FOLLOW_callExpr_in_expr634);
                    c=callExpr();

                    state._fsp--;


                     node = c; 

                    }
                    break;
                case 2 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:143:2: '@' e= callExpr
                    {
                    match(input,22,FOLLOW_22_in_expr641); 

                    pushFollow(FOLLOW_callExpr_in_expr647);
                    e=callExpr();

                    state._fsp--;


                     node = new MacroExpr(e); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:144:2: '%' e= callExpr
                    {
                    match(input,13,FOLLOW_13_in_expr654); 

                    pushFollow(FOLLOW_callExpr_in_expr660);
                    e=callExpr();

                    state._fsp--;


                     node = new PlaceExpr(e); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "expr"



    // $ANTLR start "callExpr"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:146:1: callExpr returns [Expr node] : l= baseExpr (a= args | '.' f= IDENT )* ;
    public final Expr callExpr() throws RecognitionException {
        Expr node = null;


        Token f=null;
        Expr l =null;

        Args a =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:146:29: (l= baseExpr (a= args | '.' f= IDENT )* )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:147:2: l= baseExpr (a= args | '.' f= IDENT )*
            {
            pushFollow(FOLLOW_baseExpr_in_callExpr678);
            l=baseExpr();

            state._fsp--;


             node = l; 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:147:29: (a= args | '.' f= IDENT )*
            loop12:
            do {
                int alt12=3;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==14) ) {
                    alt12=1;
                }
                else if ( (LA12_0==17) ) {
                    alt12=2;
                }


                switch (alt12) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:148:3: a= args
            	    {
            	    pushFollow(FOLLOW_args_in_callExpr690);
            	    a=args();

            	    state._fsp--;


            	     node = new Call(node, a); 

            	    }
            	    break;
            	case 2 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:149:3: '.' f= IDENT
            	    {
            	    match(input,17,FOLLOW_17_in_callExpr698); 

            	    f=(Token)match(input,IDENT,FOLLOW_IDENT_in_callExpr704); 

            	     node = new FieldGet(node, Converter.Wrap(f.getText())); 

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "callExpr"



    // $ANTLR start "baseExpr"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:152:1: baseExpr returns [Expr node] : (vr= var |vl= value );
    public final Expr baseExpr() throws RecognitionException {
        Expr node = null;


        Var vr =null;

        Value vl =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:152:29: (vr= var |vl= value )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==IDENT) ) {
                alt13=1;
            }
            else if ( ((LA13_0 >= INT && LA13_0 <= STRING)||(LA13_0 >= 32 && LA13_0 <= 33)) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }
            switch (alt13) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:153:2: vr= var
                    {
                    pushFollow(FOLLOW_var_in_baseExpr726);
                    vr=var();

                    state._fsp--;


                     node = vr; 

                    }
                    break;
                case 2 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:154:2: vl= value
                    {
                    pushFollow(FOLLOW_value_in_baseExpr737);
                    vl=value();

                    state._fsp--;


                     node = vl; 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "baseExpr"



    // $ANTLR start "args"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:157:1: args returns [Args node] : '(' (e= expr ( ',' e= expr )* )? ')' ;
    public final Args args() throws RecognitionException {
        Args node = null;


        Expr e =null;


         node = new Args(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:158:29: ( '(' (e= expr ( ',' e= expr )* )? ')' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:159:2: '(' (e= expr ( ',' e= expr )* )? ')'
            {
            match(input,14,FOLLOW_14_in_args757); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:159:6: (e= expr ( ',' e= expr )* )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0 >= IDENT && LA15_0 <= STRING)||LA15_0==13||LA15_0==22||(LA15_0 >= 32 && LA15_0 <= 33)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:159:7: e= expr ( ',' e= expr )*
                    {
                    pushFollow(FOLLOW_expr_in_args764);
                    e=expr();

                    state._fsp--;


                     node.add(e); 

                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:159:33: ( ',' e= expr )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==16) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:159:34: ',' e= expr
                    	    {
                    	    match(input,16,FOLLOW_16_in_args769); 

                    	    pushFollow(FOLLOW_expr_in_args775);
                    	    e=expr();

                    	    state._fsp--;


                    	     node.add(e); 

                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }
                    break;

            }


            match(input,15,FOLLOW_15_in_args783); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "args"



    // $ANTLR start "var"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:162:1: var returns [Var node] : t= IDENT ;
    public final Var var() throws RecognitionException {
        Var node = null;


        Token t=null;

        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:162:23: (t= IDENT )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:162:25: t= IDENT
            {
            t=(Token)match(input,IDENT,FOLLOW_IDENT_in_var799); 

             node = new Var(Converter.Wrap(t.getText())); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "var"



    // $ANTLR start "value"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:163:1: value returns [Value node] : (t= INT |t= STRING |qs= quotedStmt |qe= quotedExpr );
    public final Value value() throws RecognitionException {
        Value node = null;


        Token t=null;
        Block qs =null;

        Expr qe =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:163:27: (t= INT |t= STRING |qs= quotedStmt |qe= quotedExpr )
            int alt16=4;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt16=1;
                }
                break;
            case STRING:
                {
                alt16=2;
                }
                break;
            case 32:
                {
                alt16=3;
                }
                break;
            case 33:
                {
                alt16=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;

            }

            switch (alt16) {
                case 1 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:164:2: t= INT
                    {
                    t=(Token)match(input,INT,FOLLOW_INT_in_value816); 

                     node = new Value(Converter.Int(t.getText())); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:165:2: t= STRING
                    {
                    t=(Token)match(input,STRING,FOLLOW_STRING_in_value827); 

                     node = new Value(Converter.Unquote(t.getText())); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:166:2: qs= quotedStmt
                    {
                    pushFollow(FOLLOW_quotedStmt_in_value838);
                    qs=quotedStmt();

                    state._fsp--;


                     node = new Value(qs); 

                    }
                    break;
                case 4 :
                    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:167:2: qe= quotedExpr
                    {
                    pushFollow(FOLLOW_quotedExpr_in_value849);
                    qe=quotedExpr();

                    state._fsp--;


                     node = new Value(qe); 

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "value"



    // $ANTLR start "quotedStmt"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:170:1: quotedStmt returns [Block node] : '{:' (s= stmt )* ':}' ;
    public final Block quotedStmt() throws RecognitionException {
        Block node = null;


        Stmt s =null;


         node = new Block(); 
        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:171:30: ( '{:' (s= stmt )* ':}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:172:2: '{:' (s= stmt )* ':}'
            {
            match(input,32,FOLLOW_32_in_quotedStmt869); 

            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:172:7: (s= stmt )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0 >= IDENT && LA17_0 <= STRING)||LA17_0==13||LA17_0==22||LA17_0==25||(LA17_0 >= 27 && LA17_0 <= 33)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:172:8: s= stmt
            	    {
            	    pushFollow(FOLLOW_stmt_in_quotedStmt876);
            	    s=stmt();

            	    state._fsp--;


            	     node.stmts.add(s); 

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            match(input,18,FOLLOW_18_in_quotedStmt882); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "quotedStmt"



    // $ANTLR start "quotedExpr"
    // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:175:1: quotedExpr returns [Expr node] : '{=' e= expr '=}' ;
    public final Expr quotedExpr() throws RecognitionException {
        Expr node = null;


        Expr e =null;


        try {
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:175:31: ( '{=' e= expr '=}' )
            // C:\\Users\\mti\\Desktop\\mack\\src\\mack\\parser\\Mack.g:175:33: '{=' e= expr '=}'
            {
            match(input,33,FOLLOW_33_in_quotedExpr894); 

            pushFollow(FOLLOW_expr_in_quotedExpr900);
            e=expr();

            state._fsp--;


            match(input,21,FOLLOW_21_in_quotedExpr902); 

             node = e; 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "quotedExpr"

    // Delegated rules


 

    public static final BitSet FOLLOW_suite_in_prog64 = new BitSet(new long[]{0x000000002D400002L});
    public static final BitSet FOLLOW_module_in_suite85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_func_in_suite96 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_use_in_suite107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_my_in_suite118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_suite125 = new BitSet(new long[]{0x0000000300000E00L});
    public static final BitSet FOLLOW_callExpr_in_suite131 = new BitSet(new long[]{0x000000002D400000L});
    public static final BitSet FOLLOW_suite_in_suite137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_module157 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENT_in_module163 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_module168 = new BitSet(new long[]{0x000000042D400000L});
    public static final BitSet FOLLOW_suite_in_module175 = new BitSet(new long[]{0x000000042D400000L});
    public static final BitSet FOLLOW_34_in_module181 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_func199 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENT_in_func205 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_func210 = new BitSet(new long[]{0x0000000000008200L});
    public static final BitSet FOLLOW_var_in_func217 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_16_in_func222 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_var_in_func228 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_15_in_func236 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_block_in_func243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_use263 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENT_in_use269 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_use271 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_use277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_block298 = new BitSet(new long[]{0x00000007FA402E00L});
    public static final BitSet FOLLOW_stmt_in_block305 = new BitSet(new long[]{0x00000007FA402E00L});
    public static final BitSet FOLLOW_34_in_block311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assign_in_stmt328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ret_in_stmt339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_my_in_stmt350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifElse_in_stmt361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_block_in_stmt372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_whileLoop_in_stmt383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_stmt390 = new BitSet(new long[]{0x0000000300000E00L});
    public static final BitSet FOLLOW_callExpr_in_stmt396 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_stmt398 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_stmt405 = new BitSet(new long[]{0x0000000300000E00L});
    public static final BitSet FOLLOW_callExpr_in_stmt411 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_stmt413 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_use_in_stmt424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callExpr_in_assign443 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_20_in_assign450 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_assign456 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_assign470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ret488 = new BitSet(new long[]{0x0000000300482E00L});
    public static final BitSet FOLLOW_expr_in_ret495 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ret501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_my519 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENT_in_my525 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_my530 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_my536 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_my540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ifElse558 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_ifElse564 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_block_in_ifElse570 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_23_in_ifElse573 = new BitSet(new long[]{0x00000003FA402E00L});
    public static final BitSet FOLLOW_stmt_in_ifElse579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_whileLoop602 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_whileLoop608 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_block_in_whileLoop614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_callExpr_in_expr634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_expr641 = new BitSet(new long[]{0x0000000300000E00L});
    public static final BitSet FOLLOW_callExpr_in_expr647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_expr654 = new BitSet(new long[]{0x0000000300000E00L});
    public static final BitSet FOLLOW_callExpr_in_expr660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_baseExpr_in_callExpr678 = new BitSet(new long[]{0x0000000000024002L});
    public static final BitSet FOLLOW_args_in_callExpr690 = new BitSet(new long[]{0x0000000000024002L});
    public static final BitSet FOLLOW_17_in_callExpr698 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_IDENT_in_callExpr704 = new BitSet(new long[]{0x0000000000024002L});
    public static final BitSet FOLLOW_var_in_baseExpr726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_value_in_baseExpr737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_args757 = new BitSet(new long[]{0x000000030040AE00L});
    public static final BitSet FOLLOW_expr_in_args764 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_16_in_args769 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_args775 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_15_in_args783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_var799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_value816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_value827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_quotedStmt_in_value838 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_quotedExpr_in_value849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_quotedStmt869 = new BitSet(new long[]{0x00000003FA442E00L});
    public static final BitSet FOLLOW_stmt_in_quotedStmt876 = new BitSet(new long[]{0x00000003FA442E00L});
    public static final BitSet FOLLOW_18_in_quotedStmt882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_quotedExpr894 = new BitSet(new long[]{0x0000000300402E00L});
    public static final BitSet FOLLOW_expr_in_quotedExpr900 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_quotedExpr902 = new BitSet(new long[]{0x0000000000000002L});

}