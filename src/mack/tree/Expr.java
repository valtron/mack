package mack.tree;

import mack.exec.Dyno;
import mack.exec.ExprWalker;

public interface Expr extends Dyno
{
	public <T> T accept(ExprWalker<T> visitor);
}
