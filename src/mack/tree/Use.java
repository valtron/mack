package mack.tree;

import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.SuiteWalker;
import mack.exec.TypeError;

public class Use extends DNative implements Stmt, Suite
{
	public DString alias;
	public Expr aliased;
	public Suite resolved;
	
	public Use() {}
	
	public Use(DString alias, Expr aliased) {
		this.alias = alias;
		this.aliased = aliased;
	}

	@Override
	public void accept(SuiteWalker visitor) {
		visitor.walk(this);
	}
	
	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	@Override
	public String getName() {
		return this.alias.value;
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("alias")) return this.alias;
		if (name.equals("aliased")) return this.aliased;
		if (name.equals("resolved")) return this.resolved;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("alias")) {
			if (value instanceof DString) {
				this.alias = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("aliased")) {
			if (value instanceof Expr) {
				this.aliased = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("resolved")) {
			if (value instanceof Suite) {
				this.resolved = (Suite)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
