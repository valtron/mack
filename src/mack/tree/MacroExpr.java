package mack.tree;

import mack.exec.ExprWalker;

public class MacroExpr extends MacroBase implements Expr
{
	public MacroExpr(Expr expr) {
		super(expr);
	}
	
	@Override
	public <T> T accept(ExprWalker<T> visitor) {
		return visitor.walk(this);
	}
}
