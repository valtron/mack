package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.TypeError;

public class Assign extends DNative implements Stmt
{
	public Var var;
	public Expr value;
	
	public Assign(Var var, Expr value) {
		this.var = var;
		this.value = value;
	}
	
	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("var")) return this.var;
		if (name.equals("value")) return this.value;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("var")) {
			if (value instanceof Var) {
				this.var = (Var)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("value")) {
			if (value instanceof Expr) {
				this.value = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
