package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.TypeError;

public class If extends DNative implements Stmt {
	public Expr cond;
	public Stmt thenStmt;
	public Stmt elseStmt;
	
	public If() {}
	
	public If(Expr cond, Stmt thenStmt, Stmt elseStmt) {
		this.cond = cond;
		this.thenStmt = thenStmt;
		this.elseStmt = elseStmt;
	}

	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("cond")) return this.cond;
		if (name.equals("thenStmt")) return this.thenStmt;
		if (name.equals("elseStmt")) return this.elseStmt;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("cond")) {
			if (value instanceof Expr) {
				this.cond = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("thenStmt")) {
			if (value instanceof Stmt) {
				this.thenStmt = (Stmt)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("elseStmt")) {
			if (value instanceof Stmt) {
				this.elseStmt = (Stmt)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
