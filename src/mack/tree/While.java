package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.TypeError;

public class While extends DNative implements Stmt
{
	public Expr cond;
	public Stmt body;
	
	public While() {}
	
	public While(Expr cond, Stmt body) {
		this.cond = cond;
		this.body = body;
	}

	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("cond")) return this.cond;
		if (name.equals("body")) return this.body;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("cond")) {
			if (value instanceof Expr) {
				this.cond = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("body")) {
			if (value instanceof Stmt) {
				this.body = (Stmt)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
