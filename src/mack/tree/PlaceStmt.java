package mack.tree;

import mack.exec.StmtWalker;

public class PlaceStmt extends PlaceBase implements Stmt
{
	public PlaceStmt(Expr expr) {
		super(expr);
	}
	
	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
}
