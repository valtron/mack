package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.ExprWalker;
import mack.exec.TypeError;

public class Call extends DNative implements Expr
{
	public Expr obj;
	public Args args;
	
	public Call(Expr obj, Args args) {
		this.obj = obj;
		this.args = args;
	}

	@Override
	public <T> T accept(ExprWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("obj")) return this.obj;
		if (name.equals("args")) return this.args;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("obj")) {
			if (value instanceof Expr) {
				this.obj = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("args")) {
			if (value instanceof Args) {
				this.args = (Args)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
