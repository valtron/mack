package mack.tree;

import java.util.List;

import mack.exec.DList;

public class Args extends DList<Expr>
{
	public Args() {}
	
	public Args(List<Expr> args) {
		for (Expr e: args) {
			this.add(e);
		}
	}
}
