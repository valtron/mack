package mack.tree;

import mack.exec.ExprWalker;

public class PlaceExpr extends PlaceBase implements Expr
{
	public PlaceExpr(Expr expr) {
		super(expr);
	}
	
	@Override
	public <T> T accept(ExprWalker<T> visitor) {
		return visitor.walk(this);
	}
}
