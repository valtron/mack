package mack.tree;

import java.util.List;

import mack.exec.DList;
import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.TypeError;

public class Block extends DNative implements Stmt
{
	public DList<Stmt> stmts = new DList<Stmt>();
	
	public Block() {}
	
	public Block(List<Stmt> stmts) {
		for (Stmt s: stmts) {
			this.stmts.add(s);
		}
	}

	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("stmts")) return this.stmts;
		return super.getField(name);
	}
	
	@SuppressWarnings("unchecked")
	public void setField(String name, Dyno value) {
		if (name.equals("stmts")) {
			if (value instanceof DList<?>) {
				this.stmts = (DList<Stmt>)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
