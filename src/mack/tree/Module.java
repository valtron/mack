package mack.tree;

import mack.exec.DList;
import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.SuiteWalker;
import mack.exec.TypeError;

public class Module extends DNative implements Suite
{
	public DString name;
	public DList<Suite> items = new DList<Suite>();
	
	@Override
	public void accept(SuiteWalker visitor) {
		visitor.walk(this);
	}
	
	@Override
	public String getName() {
		return this.name.value;
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("name")) return this.name;
		if (name.equals("items")) return this.items;
		return super.getField(name);
	}
	
	@SuppressWarnings("unchecked")
	public void setField(String name, Dyno value) {
		if (name.equals("name")) {
			if (value instanceof DString) {
				this.name = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("items")) {
			if (value instanceof DList<?>) {
				this.items = (DList<Suite>)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
