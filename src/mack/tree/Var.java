package mack.tree;

import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.ExprWalker;
import mack.exec.TypeError;

public class Var extends DNative implements Expr
{
	public DString name;
	
	public Var(DString name) {
		this.name = name;
	}
	
	@Override
	public <T> T accept(ExprWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("name")) return this.name;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("name")) {
			if (value instanceof DString) {
				this.name = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
