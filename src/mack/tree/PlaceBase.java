package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.TypeError;

public class PlaceBase extends DNative
{
	public Expr expr;
	
	public PlaceBase(Expr expr) {
		this.expr = expr;
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("expr")) return this.expr;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("expr")) {
			if (value instanceof Expr) {
				this.expr = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
