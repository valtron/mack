package mack.tree;

import mack.exec.StmtWalker;

public class MacroStmt extends MacroBase implements Stmt
{
	public MacroStmt(Expr expr) {
		super(expr);
	}
	
	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
}
