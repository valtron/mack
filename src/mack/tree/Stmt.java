package mack.tree;

import mack.exec.Dyno;
import mack.exec.StmtWalker;

public interface Stmt extends Dyno
{
	public <T> T accept(StmtWalker<T> visitor);
}
