package mack.tree;

import mack.exec.Dyno;
import mack.exec.SuiteWalker;

public interface Suite extends Dyno
{
	public void accept(SuiteWalker visitor);
	
	public String getName();
}
