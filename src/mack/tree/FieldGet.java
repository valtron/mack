package mack.tree;

import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.ExprWalker;
import mack.exec.TypeError;

public class FieldGet extends DNative implements Expr
{
	public Expr obj;
	public DString field;
	
	public FieldGet(Expr obj, DString field) {
		this.obj = obj;
		this.field = field;
	}

	@Override
	public <T> T accept(ExprWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("obj")) return this.obj;
		if (name.equals("field")) return this.field;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("obj")) {
			if (value instanceof Expr) {
				this.obj = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("field")) {
			if (value instanceof DString) {
				this.field = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
