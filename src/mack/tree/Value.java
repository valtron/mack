package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.ExprWalker;
import mack.exec.TypeError;

public class Value extends DNative implements Expr
{
	public Dyno value;
	
	public Value(Dyno value) {
		this.value = value;
	}
	
	@Override
	public <T> T accept(ExprWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("value")) return this.value;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("value")) {
			if (value instanceof Dyno) {
				this.value = (Dyno)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
