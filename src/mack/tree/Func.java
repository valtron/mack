package mack.tree;

import mack.exec.DList;
import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.SuiteWalker;
import mack.exec.TypeError;

public class Func extends DNative implements Suite
{
	public DString name;
	public DList<Var> args = new DList<Var>();
	public Stmt body;
	
	@Override
	public void accept(SuiteWalker visitor) {
		visitor.walk(this);
	}
	
	@Override
	public String getName() {
		return this.name.value;
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("name")) return this.name;
		if (name.equals("args")) return this.args;
		if (name.equals("body")) return this.body;
		return super.getField(name);
	}
	
	@SuppressWarnings("unchecked")
	public void setField(String name, Dyno value) {
		if (name.equals("name")) {
			if (value instanceof DString) {
				this.name = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("args")) {
			if (value instanceof DList<?>) {
				this.args = (DList<Var>)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("body")) {
			if (value instanceof Stmt) {
				this.body = (Stmt)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
