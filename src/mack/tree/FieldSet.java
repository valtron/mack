package mack.tree;

import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.TypeError;

public class FieldSet extends DNative implements Stmt
{
	public Expr obj;
	public DString field;
	public Expr value;
	
	public FieldSet(Expr obj, DString field, Expr value) {
		this.obj = obj;
		this.field = field;
		this.value = value;
	}

	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("obj")) return this.obj;
		if (name.equals("field")) return this.field;
		if (name.equals("value")) return this.value;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("obj")) {
			if (value instanceof Expr) {
				this.obj = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("field")) {
			if (value instanceof DString) {
				this.field = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("value")) {
			if (value instanceof Expr) {
				this.value = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
