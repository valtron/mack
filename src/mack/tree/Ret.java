package mack.tree;

import mack.exec.DNative;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.TypeError;

public class Ret extends DNative implements Stmt {
	public Expr value;
	
	public Ret() {}
	
	public Ret(Expr value) {
		this.value = value;
	}
	
	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("value")) return this.value;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("value")) {
			if (value instanceof Expr) {
				this.value = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
