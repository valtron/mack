package mack.tree;

import mack.exec.DNative;
import mack.exec.DString;
import mack.exec.Dyno;
import mack.exec.StmtWalker;
import mack.exec.SuiteWalker;
import mack.exec.TypeError;

public class My extends DNative implements Stmt, Suite
{
	public DString varName;
	public Expr value;
	
	public My() {}
	
	public My(DString varName, Expr value) {
		this.varName = varName;
		this.value = value;
	}
	
	@Override
	public void accept(SuiteWalker visitor) {
		visitor.walk(this);
	}
	
	@Override
	public <T> T accept(StmtWalker<T> visitor) {
		return visitor.walk(this);
	}
	
	@Override
	public String getName() {
		return this.varName.value;
	}
	
	//#dynogen begin
	public Dyno getField(String name) {
		if (name.equals("varName")) return this.varName;
		if (name.equals("value")) return this.value;
		return super.getField(name);
	}
	
	public void setField(String name, Dyno value) {
		if (name.equals("varName")) {
			if (value instanceof DString) {
				this.varName = (DString)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		if (name.equals("value")) {
			if (value instanceof Expr) {
				this.value = (Expr)value;
				return;
			}
			throw TypeError.SetInvalidType(name);
		}
		super.setField(name, value);
	}
	//#dynogen end
}
