package mack.tree;

import mack.exec.SuiteWalker;

public class MacroSuite extends MacroBase implements Suite
{
	public MacroSuite(Call expr) {
		super(expr);
	}
	
	@Override
	public void accept(SuiteWalker visitor) {
		visitor.walk(this);
	}

	@Override
	public String getName() {
		assert this.expr instanceof Call;
		Call c = (Call)this.expr;
		
		assert c.args.size() > 0;
		Expr e = c.args.get(0);
		
		assert e instanceof Suite;
		return ((Suite)e).getName();
	}
}
